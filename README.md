Install Java from:
	https://www.java.com/en/download/

Install Java FX from:
	https://gluonhq.com/products/javafx/

Don't forget to set the environment variable PATH_TO_FX to the install directory of JAVA FX. On my machine I have set as follows in ~/.zshrc:
	export PATH_TO_FX=~/opt/javafx-sdk-15.0.1/lib;

Uncompress the tar.gz and cd into the bin directory created and execute the script 'soqlplus3'.


