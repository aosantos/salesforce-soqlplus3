package com.app.soqlplus3;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;

import java.lang.management.ManagementFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import java.net.InetAddress;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.UnknownHostException;

import java.nio.file.Files;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Util {
	public static Boolean isEmpty(String str) {
		return str == null || str != null && str.trim().isEmpty();
	}
	
	/** Get local hostname (use trimDomainName to select between without domain name (true) or with domain name (false) **/
	public static String getLocalHostName(Boolean trimDomainName) {
		String result = null;
		
		//	NOTE -- InetAddress.getLocalHost().getHostName() will not work in certain environments.
		try {
		    result = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
		    // failed;  try alternate means.
		}
	
		if (result == null || result.isEmpty()) {
			// try environment properties.
			result = System.getenv("COMPUTERNAME");

			if (result == null || result.isEmpty()) {
				result = System.getenv("HOSTNAME");
			}
		}
		
		if (result != null && result.isEmpty() == false) {
			if (trimDomainName) {
				int firstIndexOfDot = result.indexOf(".");
						
				if (firstIndexOfDot != -1) {
					result = result.substring(0, firstIndexOfDot);
				}
			}
		}
		
		return result;
	}
	
	/** Get local hostname including domain name **/
	public static String getLocalHostName() {
		return getLocalHostName(false);
	}
	
	/** Get current process ID **/
	public static String getProcessId() {
		String processID = null;
		
		processID = ManagementFactory.getRuntimeMXBean().getName();
		
		if (processID != null) {
			int indexOfAt = processID.indexOf("@");
			
			if (indexOfAt != -1) {
				processID = processID.substring(0, indexOfAt);
			}
		}
		
		return processID;
	}
	
	/** Get current executing thread ID **/
	public static String getThreadId() {
		String threadId = null;
		Thread currentT = Thread.currentThread();
		
		if (currentT != null) {
			threadId = String.valueOf(currentT.getId());
		}
		
		return threadId;
	}
	
	/** Get unique ID **/
	/*
	public static String getUniqueId() {
		String localHostName = String.format("%x",  new BigInteger(1, getLocalHostName(true).getBytes()));
		String processId = getProcessId();
		String threadId = getThreadId();
		String nanoSecsHex = String.format("%x", System.nanoTime());
		String result = "";
		
		result = nanoSecsHex + "_" + localHostName + "_" + processId + "_" + threadId;
		
		return result;
	}
	*/
	
	/** Get a Simple Unique ID **/
	public static String getSimpleUniqueId() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
		Date currentDate = new Date();
		String processId = getProcessId();
		String threadId = getThreadId();
		String simpleUniqueId = simpleDateFormat.format(currentDate) + "_" + processId + "_" + threadId;
		
		return simpleUniqueId;
	}
	
	/** Get a String dump of a Throwable Stack Trace **/
	public static String getStackTraceString(Throwable t) {
		if (t != null) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			
			t.printStackTrace(pw);
			
			return sw.toString();
		}
		
		return null;
	}
	
	/** Get a String dump of the current Thread Stack Trace **/
	public static String getCurrentThreadStackTraceString() {
		Thread thread = Thread.currentThread();
		
		return getStackTraceString(thread.getStackTrace());
	}
	
	/** Get a String dump of a Thread Stack Trace **/
	public static String getStackTraceString(StackTraceElement[] stackTraceElementArray) {
		String result = "";
		
		for (StackTraceElement traceElement : stackTraceElementArray) {
			if (result.isEmpty() == false) {
				result += "\n";
			}
			
			result += traceElement;
		}
		
		return result;
	}
	
	/** Create a Directory **/
	public static void createDirectory(String pathname) {
		Boolean isDirectoryFound = false;
		File path = new File(pathname);
		
		isDirectoryFound = path.exists();
		
		if (! isDirectoryFound) {
			Boolean result = path.mkdirs();
			
			if (! result) {
				throw new RuntimeException("Error creating directory \'" + pathname + "'");
			}
		}
	}
	
	/** Check if a Directory has files **/
	public static Boolean hasFiles(String pathname) {
		Boolean filesFound = false;
		File path = new File(pathname);
		
		if (path.exists()) {
			String[] files = path.list();
			
			if (files != null && files.length > 0) {
				for (String filename : files) {
					String testFilename = pathname + "/" + filename;
					File testFile = new File(testFilename);
					
					if (testFile.isFile()) {
						filesFound = true;
						break;
					}
				}
			}
		}
		
		//System.out.println("hasFiles(" + pathname + ")=" + filesFound);
		
		return filesFound;
	}
	
	/** Move a Path (either file or directory) to a new name **/
	public static void rename(String pathname, String targetPathname) {
		File sourcePath = new File(pathname);
		File targetPath = new File(targetPathname);
		
		if (! sourcePath.renameTo(targetPath)) {
			throw new RuntimeException("Error moving \'" + pathname + "\' to \'" + targetPathname + "\'");
		}
	}
	
	public static String getMapAsString(Map<String,Object> context) {
		List<String> keyList = new Vector<String>();
		String result = null;
		
		if (context != null) {
			keyList.addAll(context.keySet());
			Collections.sort(keyList);
			
			for (String key : keyList) {
				if (result == null) {
					result = "";
				} else {
					result += "\n";
				}
				
				result += key + ": \'" + context.get(key) + "\'";
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	public static String inspectObject(Object zobject) {
		try {
			String result = "";
			Class zclass;
			
			if (zobject != null) {
				Field[] objectFields;
				
				zclass = zobject.getClass();
				
				objectFields = zclass.getDeclaredFields();
				
				result = "{\n";
				
				if (objectFields != null && objectFields.length > 0) {
					for (Field field : objectFields) {
						Object fieldValue = "null";
						
						fieldValue = field.get(zobject);
						
						if (fieldValue == null) {
							fieldValue = "null";
						}
						
						result += "\t" + field.getName() + ": " + fieldValue + "\n";
					}
				}
				
				result += "}\n";
			} else {
				result = "null";
			}
			
			return result;
		} catch (Exception e) {
			return Util.getStackTraceString(e);
		}
	}
	
	@SuppressWarnings("rawtypes")
	public static List<String> getClassPublicFields(Class zclass) {
		List<String> publicFieldList = new Vector<String>();
		
		if (zclass != null) {
			Field[] fields = zclass.getDeclaredFields();
			
			if (fields != null && fields.length > 0) {
				for (int i = 0; i < fields.length; i++) {
					Field field = fields[i];
					
					publicFieldList.add(field.getName());
				}
			}
		}
		
		return publicFieldList;
	}
	
	@SuppressWarnings("rawtypes")
	public static Field getField(Class zclass, String fieldName) {
		Field result = null;
		
		try {
			result = zclass.getField(fieldName);
		} catch (Exception e) {
			;
		}
		
		return result;
	}
	public static Method getGetter(Class<?> zclass, String fieldName, Class<?> returnType) {
		String getMethodName = "get" + fieldName.substring(0,1).toUpperCase() + fieldName.substring(1);
		Method method = null;
		Method[] classDeclaredMethods;
		
		try {
			classDeclaredMethods = zclass.getDeclaredMethods();
			
			if (classDeclaredMethods != null && classDeclaredMethods.length != 0) {
				for (int i = 0; i < classDeclaredMethods.length; i++) {
					Method classMethod = classDeclaredMethods[i];
					
					if (classMethod.getName().equals(getMethodName) && (returnType == null || classMethod.getReturnType().equals(returnType))) {
						method = classMethod;
						break;
					}
				}
			}
		} catch (Exception e) {
		}
		
		//System.out.println("Returning method '" + method.getName() + ", ReturnType '" + method.getReturnType() + "'");
		
		return method;
	}

	public static Method getSetter(Class<?> zclass, String fieldName, Class<?> fieldType) {
		String setMethodName = "set" + fieldName.substring(0,1).toUpperCase() + fieldName.substring(1);
		Method method = null;
		
		try {
			//System.out.println("setMethod: " + setMethodName);
			
			method = zclass.getDeclaredMethod(setMethodName, fieldType);
			
			if (method == null) {
				System.out.println("cannot get method");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return method;
	}
	
	public static Boolean parseBoolean(Object v) {
		if (v != null && ((String)v).isEmpty() == false) {
			return Boolean.parseBoolean((String)v);
		} else {
			return null;
		}
	}
	
	public static Double parseDouble(Object v) {
		if (v != null && ((String)v).isEmpty() == false) {
			return Double.parseDouble((String)v);
		} else {
			return null;
		}
	}
	
	public static Integer parseInteger(Object v) {
		if (v != null && ((String)v).isEmpty() == false) {
			return Integer.parseInt((String)v);
		} else {
			return null;
		}
	}
	
	public static Date parseDatetime(Object v) throws ParseException {
		if (v != null && ((String)v).isEmpty() == false) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			return simpleDateFormat.parse((String)v);
		} else {
			return null;
		}
	}
	
	public static Date parseDate(Object v) throws ParseException {
		if (v != null && ((String)v).isEmpty() == false) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			
			return simpleDateFormat.parse((String)v);
		} else {
			return null;
		}
	}
	
	public static String parseString(Object v) {
		if (v != null && ((String)v).isEmpty() == false) {
			return (String)v;
		} else {
			return null;
		}
	}
	
	public static String getFileContent(String filename) throws IOException {
		Reader reader = null;
		String content;
		StringBuilder stringBuilder = new StringBuilder();
		
		if (filename != null && filename.equals("-")) {
			Scanner stdin = new Scanner(System.in);
			
			while (stdin.hasNextLine()) {
				String line = stdin.nextLine();
				
				stringBuilder.append(line);
			}
			
			stdin.close();
		} else {
			if (! Files.isReadable(new File(filename).toPath())) {
				throw new RuntimeException("FATAL: Cannot read from file " + filename);
			}
			
			try {
				reader = new BufferedReader(new FileReader(filename));
				char[] buffer = new char[1024];
				int r = 0;
				
				while ((r = reader.read(buffer)) != -1) {
					stringBuilder.append(buffer, 0, r);
				}
			} finally {
				if (reader != null) {
					reader.close();
				}
			}
		}
		
		content = stringBuilder.toString();
		
		return content;
	}
	
	/**
	 * Get file content as byte array.
	 * 
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	public static byte[] getFileContentAsByteArray(String filename) throws IOException {
		String fileContent = getFileContent(filename);
		byte[] result = fileContent.getBytes();
		
		return result;
	}
	
	/** Get the current Date of the System **/
	public static Date getCurrentDate() {
		return new Date();
	}
	
	/**
	 * 
	 * Zip a File
	 *  
	 */
	public static void zip(String zipFile, String sourceDirectory) throws Exception {
		List<String> fileList = new ArrayList<>();
		byte[] buffer = new byte[4096];
		FileOutputStream fileOutputStream;
		ZipOutputStream zipOutputStream;
		
		zipGenerateFileList(sourceDirectory, fileList, new File(sourceDirectory));
		
		fileOutputStream = new FileOutputStream(zipFile);
		zipOutputStream = new ZipOutputStream(fileOutputStream);
		
		for (String file : fileList) {
			ZipEntry zipEntry = new ZipEntry(file);
			FileInputStream fileInputStream;
			int len;
			
			zipOutputStream.putNextEntry(zipEntry);
			
			fileInputStream = new FileInputStream(sourceDirectory + File.separator + file);
			
			while ((len = fileInputStream.read(buffer)) > 0) {
				zipOutputStream.write(buffer, 0, len);
			}
			
			fileInputStream.close();
		}
		
		zipOutputStream.closeEntry();
		zipOutputStream.close();
	}
	
	/**
	 * 
	 * Zip a File
	 *  
	 */
	public static void zip(String zipFile, String sourceDirectory, Set<String> ignoredFolders) throws Exception {
		List<String> fileList = new ArrayList<>();
		byte[] buffer = new byte[4096];
		FileOutputStream fileOutputStream;
		ZipOutputStream zipOutputStream;
		
		zipGenerateFileList(sourceDirectory, fileList, new File(sourceDirectory), ignoredFolders);
		
		fileOutputStream = new FileOutputStream(zipFile);
		zipOutputStream = new ZipOutputStream(fileOutputStream);
		
		for (String file : fileList) {
			ZipEntry zipEntry = new ZipEntry(file);
			FileInputStream fileInputStream;
			int len;
			
			zipOutputStream.putNextEntry(zipEntry);
			
			if (sourceDirectory.equals(".") && file.startsWith("/")) {
				fileInputStream = new FileInputStream(file);
			} else {
				fileInputStream = new FileInputStream(sourceDirectory + File.separator + file);
			}
			
			while ((len = fileInputStream.read(buffer)) > 0) {
				zipOutputStream.write(buffer, 0, len);
			}
			
			fileInputStream.close();
		}
		
		zipOutputStream.closeEntry();
		zipOutputStream.close();
	}
	
	/**
	 * 
	 * zipGenerateFileList.
	 * 
	 * @param source
	 * @param fileList
	 * @param node
	 */
	private static void zipGenerateFileList(String source, List<String> fileList, File node) {
		if (node.isFile()) {
			fileList.add(zipGenerateZipEntry(source, node.getAbsoluteFile().toString()));
		}
		
		if (node.isDirectory()) {
			String[] subNode = node.list();
			
			for (String filename : subNode) {
				zipGenerateFileList(source, fileList, new File(node, filename));
			}
		}
	}
	
	/**
	 * zipFolderIsIgnored.
	 * 
	 * @param filename
	 * @param ignoredFolders
	 * @return
	 */
	private static boolean zipFolderIsIgnored(String filename, Set<String> ignoredFolders) {
		boolean result = false;
		
		if (ignoredFolders != null && ignoredFolders.isEmpty() == false) {
			for (String ignoredFolder : ignoredFolders) {
				if (filename.startsWith(ignoredFolder)) {
					result = true;
					break;
				}
			}
		}
		
		return result;
	}
	
	/**
	 * zipGenerateFileList.
	 * 
	 * @param source
	 * @param fileList
	 * @param node
	 * @param ignoredFolders
	 */
	private static void zipGenerateFileList(String source, List<String> fileList, File node, Set<String> ignoredFolders) {
		if (node.isFile()) {
			String fileName = zipGenerateZipEntry(source, node.getAbsoluteFile().toString());
			
			if (zipFolderIsIgnored(fileName, ignoredFolders) == false) {
				fileList.add(fileName);
			}
		}
		
		if (node.isDirectory()) {
			String[] subNode = node.list();
			
			for (String filename : subNode) {
				zipGenerateFileList(source, fileList, new File(node, filename), ignoredFolders);
			}
		}
	}
	
	/**
	 * zipGenerateZipEntry.
	 * 
	 * @param source
	 * @param file
	 * @return
	 */
	private static String zipGenerateZipEntry(String source, String file) {		
		if (file.startsWith(source)) {
			return file.substring(source.length() + 1, file.length());
		} else if (source.equals(".") && file.startsWith("/")){
			return file;
		} else {
			return source + "/" + file;
		}
	}
		
	/** 
	 * 
	 * Unzip a File
	 *  
	 */
	public static void unzip(String zipFile, String targetDirectory) throws Exception {
		ZipInputStream zipIn;
		ZipEntry zipEntry;
		File target = new File(targetDirectory);
		
		if (target.exists() == false) {
			Util.mkdir(targetDirectory);
		}
		
		zipIn = new ZipInputStream(new FileInputStream(zipFile));
		zipEntry = zipIn.getNextEntry();
		
		while (zipEntry != null) {
			String filePath = targetDirectory + File.separator + zipEntry.getName();
			
			if (zipEntry.isDirectory() == false) {
				BufferedOutputStream bufferedOutputStream;
				byte[] bytesIn = new byte[4096];
				int read = 0;
				
				// Extract file
				if (Util.directoryExists(filePath) == false) {
					Util.mkdir(dirname(filePath));
				}
				
				bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(filePath));
				while ((read = zipIn.read(bytesIn)) != -1) {
					bufferedOutputStream.write(bytesIn, 0, read);
				}
				bufferedOutputStream.close();
			} else {
				// Create directory
				if (Util.directoryExists(filePath) == false) {
					Util.mkdir(filePath);
				}
			}
			
			zipIn.closeEntry();
			zipEntry = zipIn.getNextEntry();
		}
		
		zipIn.close();
	}
	
	/**
	 * 
	 * Check if directory exists
	 *  
	 */
	public static boolean directoryExists(String pathname) {
		File file = new File(pathname);
		
		return file.exists() && file.isDirectory();
	}
	
	/**
	 * 
	 * Create a directory (including all the intermediate ones)
	 * 
	 *  
	 */
	public static void mkdir(String pathname) {
		File directory = new File(pathname);
		
		directory.mkdirs();
		
		if (directoryExists(directory.getAbsolutePath()) == false) {
			throw new RuntimeException("Error creating directory \'" + pathname + "\'");
		}
	}
	
	/**
	 * 
	 * Get dirname
	 *  
	 */
	public static String dirname(String filePath) {
		int lastIndexOfSeparator = filePath.lastIndexOf('/');
		return filePath.substring(0, lastIndexOfSeparator);
	}
	
	/** 
	 * 
	 * Check if file exists
	 *  
	 */
	public static boolean fileExists(String pathname) {
		File file = new File(pathname);
		
		return file.exists() && file.isDirectory() == false;
	}
	
	/** 
	 * 
	 * Check if file or directory exists
	 *  
	 */
	public static boolean fileOrDirectoryExists(String pathname) {
		return fileExists(pathname) || directoryExists(pathname);
	}
	
	/**
	 * 
	 * Remove a file
	 *  
	 */
	public static void rmfile(String pathname) {
		File filename = new File(pathname);
		
		if (filename.delete() == false) {
			throw new RuntimeException("Error deleting file \'" + pathname +"\'");
		}
	}
	
	/**
	 * 
	 * Remove a directory
	 *  
	 */
	public static void rmdir(String pathname, boolean recursive) {
		File f = new File(pathname);
		
		if (f.isDirectory() && recursive) {
			for (File c : f.listFiles()) {
				rmdir(c.getAbsolutePath(), recursive);
			}
		}
		
		if (f.delete() == false) {
			throw new RuntimeException("Error deleting file \'" + f.getAbsolutePath() + "\'");
		}
	}
	
	/**
	 * 
	 * Move a file
	 *  
	 */
	public static void mvfile(String from, String to) {
		File source = new File(from);
		File target = new File(to);
		
		if (source.renameTo(target) == false) {
			throw new RuntimeException("Error moving file \'" + from + "\' to \'" + to + "\'");
		}
	}
	
	/**
	 * 
	 * Copy a file, replacing if exists
	 *  
	 */
	public static void fileCopy(String src, String dst) throws IOException {
		File srcFile = new File(src);
		File dstFile = new File(dst);
		
		Files.copy(srcFile.toPath(), dstFile.toPath(), REPLACE_EXISTING);
	}
	
	/**
	 * 
	 * Copy a file, replacing if exists
	 *  
	 */
	public static void fileCopy(InputStream stream, String dst) throws IOException {
		File dstFile = new File(dst);
		
		Files.copy(stream, dstFile.toPath(), REPLACE_EXISTING);
	}
	
	/**
	 * 
	 * Check if file exists and is read accessible by app
	 *  
	 */
	public static boolean fileReadable(String filename) {
		boolean result = false;
		File targetFile = new File(filename);
		
		if (targetFile.exists() && targetFile.isDirectory() == false && targetFile.canRead()) {
			result = true;
		}
		
		return result;
	}
	
	/** Check if file exists and is write accessible by app */
	public static boolean fileWritable(String filename) {
		boolean result = false;
		File targetFile = new File(filename);
		
		if (targetFile.exists() && targetFile.isDirectory() == false && targetFile.canWrite()) {
			result = true;
		}
		
		return result;
	}
	
	/**
	 * 
	 * Create a file with content from a String 
	 *  
	 */
	public static void createFileWithContent(String filename, String content) throws IOException {
		File file = new File(filename);
		String parentDirName = file.getParent();

		if (parentDirName != null) {
			mkdir(parentDirName);
		}
		
		FileOutputStream fileOutputStream = new FileOutputStream(file);
		
		fileOutputStream.write(content.getBytes());
		
		fileOutputStream.close();
	}
	
	/**
	 * Create a ZIP file from a list of XmlFiles.
	 * 
	 * @param xmlFileList
	 * @return
	 * @throws Exception 
	 */
	public static String getZipFile(List<XmlFile> xmlFileList) throws Exception {
		String uniqueId = getUniqueId();
		String temporaryDirectory = File.separator + "tmp" + File.separator + uniqueId;
		String temporaryZipFile = File.separator + "tmp" + File.separator + uniqueId + ".zip";
		
		if (fileOrDirectoryExists(temporaryDirectory)) {
			Util.rmdir(temporaryDirectory, true);
		}
		
		if (fileExists(temporaryZipFile)) {
			Util.rmfile(temporaryZipFile);
		}
		
		Util.mkdir(temporaryDirectory);
		
		for (XmlFile xmlFile : xmlFileList) {
			String pathname = temporaryDirectory + (isEmpty(xmlFile.directory) ? "" : File.separator + xmlFile.directory);
			String filename = pathname + File.separator + xmlFile.filename;
			
			createFileWithContent(filename, xmlFile.content);
		}
		
		Util.zip(temporaryZipFile, temporaryDirectory);
		
		return temporaryZipFile;
	}
	
	/**
	 * Get a unique identifier that can be used as filename in filesystem or other kind of resource.
	 * 
	 * @return
	 */
	public static String getUniqueId() {
		String nanoSecondsHexadecimal = String.format("%x", System.nanoTime());
		long randomLong = ThreadLocalRandom.current().nextLong();
		String identifier = "";
		
		identifier = nanoSecondsHexadecimal + String.format("%x", randomLong);
		
		return identifier;
	}
	
	/**
	 * Read the bytes from a zip file content.
	 * 
	 * @param zipFilename
	 * @return
	 * @throws Exception
	 */
	public static byte[] readZipFile(String zipFilename) throws Exception {
		byte[] result = null;
		File zipFile = new File(zipFilename);
		FileInputStream fileInputStream = new FileInputStream(zipFile);
		
		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			byte[] buffer = new byte[4096];
			int bytesRead = 0;
			
			while (-1 != (bytesRead = fileInputStream.read(buffer))) {
				byteArrayOutputStream.write(buffer, 0, bytesRead);
			}
			
			result = byteArrayOutputStream.toByteArray();
		} finally {
			fileInputStream.close();
		}
		
		return result;
	}
	
	/**
	 * 
	 * Get List of Files in Directory 
	 *  
	 */
	public static List<String> getDirectoryFiles(String root) throws Exception {
		File folder = new File(root);
		List<String> files = new ArrayList<String>();
		
		for (File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				List<String> subFolderFiles = getSubDirectoryFiles(root, fileEntry.getName());
				
				if (subFolderFiles != null && subFolderFiles.isEmpty() == false) {
					files.addAll(subFolderFiles);
				}
			} else {
				files.add(fileEntry.getName());
			}
		}
		
		return files;
	}
	
	/**
	 * 
	 * Get List of Files from a subDirectory of Root, returning the sub part prefixed in each file name.
	 *  
	 */
	public static List<String> getSubDirectoryFiles(String root, String sub) throws Exception {
		List<String> files = new ArrayList<String>();
		File folder = new File(root + File.separator + sub);
		
		for (File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				List<String> subFolderFiles = getSubDirectoryFiles(root, sub + File.separator + fileEntry.getName());
				
				if (subFolderFiles != null && subFolderFiles.isEmpty() == false) {
					files.addAll(subFolderFiles);
				}
			} else {
				files.add(sub + File.separator + fileEntry.getName());
			}
		}
		
		return files;
	}
	
	/**
	 * 
	 * Get a file content from a resource
	 * 
	 */
	public static InputStream getResourceInputStream(String filename) throws Exception {
		URL fileURL = ClassLoader.getSystemResource(filename);
		
		if (fileURL != null) {
			return fileURL.openStream();
		}
		
		throwError("Error finding file", filename);
		
		return null;
	}
	
	/**
	 * 
	 * Throw an error showing additionally the runtime classpath
	 * 
	 */
	public static void throwError(String prefix, String argument) throws Exception {
		URL[] urls = ((URLClassLoader)ClassLoader.getSystemClassLoader()).getURLs();
		List<String> paths = new ArrayList<>();
		
		for (URL url : urls) {
			paths.add(url.toExternalForm());
		}
		
		String extra = "\nClassLoader Path:\n\t" + String.join("\n\t", paths);
		
		throw new RuntimeException(prefix + ": " + argument + extra);
	}
}
