package com.app.soqlplus3.model;

public class BigObjectCustomField {
	public enum FieldType {
		DateTime,
		Lookup,
		Number,
		Text,
		LongTextArea
	};
	
	public String label;
	public String pluralLabel;
	public String fullName;
	public FieldType type;
	public Integer scale;
	public Integer precision;
	public Integer length;
	public String referenceTo;
	public String relationshipName;
	public Boolean required;
	public Boolean externalId;
	public Boolean unique;
	
	public BigObjectCustomField() {
		label = new String();
		pluralLabel = new String();
		fullName = new String();
		type = FieldType.Text;
		scale = 0;
		precision = 0;
		length = 0;
		referenceTo = new String();
		relationshipName = new String();
		required = false;
		externalId = false;
		unique = false;
	}
}
