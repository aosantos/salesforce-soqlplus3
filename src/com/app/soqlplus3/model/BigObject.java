package com.app.soqlplus3.model;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class BigObject {
	public String label;
	public String pluralLabel;
	public String fullName;
	
	public List<BigObjectCustomField> fields;
	public List<Index> indexes;
	
	public BigObject() {
		fields = new ArrayList<>();
		indexes = new ArrayList<>();
	}
	
	public static BigObject parseXml(String fullName, String xml) throws SAXException, IOException, ParserConfigurationException {
		BigObject bo = new BigObject();
		
		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Element d = db.parse(new ByteArrayInputStream(xml.getBytes())).getDocumentElement();
		
		bo.fullName = fullName;
		
		for (Node c = d.getFirstChild(); c != null; c = c.getNextSibling()) {
			if (c instanceof Element) {
				String nn = c.getNodeName();
				
				if (nn.equals("label")) {
					bo.label = c.getTextContent();
				} else if (nn.equals("pluralLabel")) {
					bo.pluralLabel = c.getTextContent();
				} else if (nn.equals("fields")) {
					// Parse field
					NodeList fieldNodes = c.getChildNodes();
					BigObjectCustomField customField = new BigObjectCustomField();
					
					for (int i = 0; i < fieldNodes.getLength(); i++) {
						Node fn = fieldNodes.item(i);
						
						if (fn instanceof Element) {
							Element fne = (Element)fn;
							
							String fnen = fne.getNodeName();
							
							if (fnen.equals("fullName")) {
								customField.fullName = fne.getTextContent();
							} else if (fnen.equals("externalId")) {
								customField.externalId = Boolean.valueOf(fne.getTextContent());
							} else if (fnen.equals("label")) {
								customField.label = fne.getTextContent();
							} else if (fnen.equals("pluralLabel")) {
								customField.pluralLabel = fne.getTextContent();
							} else if (fnen.equals("precision")) {
								customField.precision = Integer.valueOf(fne.getTextContent());
							} else if (fnen.equals("scale")) {
								customField.scale = Integer.valueOf(fne.getTextContent());
							} else if (fnen.equals("referenceTo")) {
								customField.referenceTo = fne.getTextContent();
							} else if (fnen.equals("relationshipName")) {
								customField.relationshipName = fne.getTextContent();
							} else if (fnen.equals("length")) {
								customField.length = Integer.valueOf(fne.getTextContent());
							} else if (fnen.equals("required")) {
								customField.required = Boolean.valueOf(fne.getTextContent());
							} else if (fnen.equals("type")) {
								customField.type = BigObjectCustomField.FieldType.valueOf(fne.getTextContent());
							} else if (fnen.equals("unique")) {
								customField.unique = Boolean.valueOf(fne.getTextContent());
							}
						}
					}
					
					bo.fields.add(customField);
				} else if (nn.equals("indexes")) {
					NodeList ic = c.getChildNodes();
					
					for (int j = 0; j < ic.getLength(); j++) {
						Node jn = ic.item(j);
						
						if (jn instanceof Element) {
							Element jne = (Element)jn;
							
							String jnen = jne.getNodeName();
							
							if (jnen.equals("fields")) {
								// Parse index
								NodeList indexNodes = jne.getChildNodes();
								Index index = new Index();
								
								for (int i = 0; i < indexNodes.getLength(); i++) {
									Node in = indexNodes.item(i);
									
									if (in instanceof Element) {
										Element ine = (Element)in;
										
										String inen = ine.getNodeName();
										
										if (inen.equals("name")) {
											index.name = ine.getTextContent();
										} else if (inen.equals("sortDirection")) {
											index.sortDirection = Index.SortDirection.valueOf(ine.getTextContent());
										}
									}
								}
								
								bo.indexes.add(index);
							}
						}
					}
				}
			}
		}
		
		return bo;
	}
}
