package com.app.soqlplus3.model;

public class Index {
	public enum SortDirection {
		ASC,
		DESC
	};
	
	public String name;
	public SortDirection sortDirection;
	
	public Index() {
		name = new String();
		sortDirection = SortDirection.ASC;
	}
}
