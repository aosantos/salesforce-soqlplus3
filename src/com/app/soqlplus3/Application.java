package com.app.soqlplus3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.opencsv.CSVWriter;
import com.sforce.soap.metadata.FileProperties;
import com.sforce.ws.ConnectionException;

import javafx.concurrent.Task;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Taskbar;
import java.awt.Toolkit;
import java.net.URL;

import com.app.soqlplus3.dialog.AboutDialog;
import com.app.soqlplus3.dialog.BigObjectDialog;
import com.app.soqlplus3.dialog.CustomObjectSelectDialog;
import com.app.soqlplus3.dialog.ConfirmationDialog;
import com.app.soqlplus3.dialog.ExceptionDialog;
import com.app.soqlplus3.dialog.ProgressDialog;
import com.app.soqlplus3.dialog.SalesforceConnectionDialog;
import com.app.soqlplus3.dialog.SalesforceDescribeDialog;
import com.app.soqlplus3.model.BigObject;

public class Application extends javafx.application.Application {
	public static final String APPLICATION_TITLE = "soqlplus3 1.0002";
	public static final int MAIN_WINDOW_WIDTH = 1000;
	public static final int MAIN_WINDOW_HEIGHT = 600;
	public static final int TABLEVIEW_MAX_HEIGHT = 1600;
	
	public static final int INPUT_QUERY_ROWS = 16;
	public static final String TEXT_HEADER_STYLE = "-fx-font: 18px Verdana;";
	public static final String ICON_NAME = "soqlplus3.png";
	public static final String ROW_NUM = "Row#";
	
	public class SalesforceAsyncQuery implements Runnable {
		public Application app;
		public Task<Void> task;
		
		SalesforceAsyncQuery(Application app) {
			this.app = app;
		}
		
		@Override
		public void run() {
			task = SalesforceAsync.doQuery(app, salesforce, app.textAreaQuery.getText());
		}
	}

	Boolean isConnected = false;
	Boolean isQueryRun = false;
	String connectionName;
	Connections connections;
	Salesforce salesforce;
	String connectedUsername;
	Boolean connectedToProduction;
	String envDebug = System.getenv("DEBUG");

	SalesforceAsyncQuery asyncQuery;
	
	List<String> selectedFieldList;

	String openFilename = null;

	// UI
	Stage stage;

	// UI Buttons
	Button buttonAbout = new Button("About...");
	Button buttonConnect = new Button("Connect...");
	Button buttonLoad = new Button("Load...");
	Button buttonSave = new Button("Save");
	Button buttonSaveAs = new Button("Save As...");
	Button buttonRunQuery = new Button("Run Query");
	Button buttonExportAs = new Button("Export As...");
	Button buttonDescribe = new Button("Describe...");
	Button buttonNewBigObject = new Button("New Big Object...");
	Button buttonEditBigObject = new Button("Edit Big Object...");

	ProgressDialog executeQueryProgressDialog;
	
	// UI Inputs
	TextArea textAreaQuery = new TextArea();

	// UI Outputs
	TableView<QueryResultRecord> tableView = new TableView<QueryResultRecord>();

	Text connectedTo = new Text();

	class FilePropertiesComparable implements Comparable<FilePropertiesComparable> {
		FileProperties fp;

		public FilePropertiesComparable(FileProperties fp) {
			this.fp = fp;
		}

		@Override
		public int compareTo(Application.FilePropertiesComparable o) {
			if (o != null) {
				return this.fp.getFullName().compareTo(o.fp.getFullName());
			} else {
				return -1;
			}
		}
	}

	static Integer staticRowNum = 0;

	class QueryResultRecord {
		private Map<String, Object> record;
		private Integer rowNum;

		public QueryResultRecord(Map<String, Object> record) {
			this.record = record;

			rowNum = ++staticRowNum;
		}

		public Object getValue(String fieldName) {
			return record.get(fieldName);
		}

		public Integer getRowNum() {
			return rowNum;
		}

		public String toString() {
			String result = "";
			int counter = 0;

			for (String key : record.keySet()) {
				counter++;

				if (counter > 1) {
					result += ", ";
				}

				result += key + ": " + record.get(key);
			}

			return result;
		}
	}

	public static void main(String[] args)
			throws ConnectionException, ParseException, FileNotFoundException, IOException {
		launch(args);
	}

	public String masquerade(String field) {
		String masqueraded = "";

		for (int i = 0; i < field.length(); i++) {
			String character = field.substring(i, i + 1);

			if (i > 0 && i < field.length() - 1) {
				character = "*";
			}

			masqueraded += character;
		}

		return masqueraded;
	}

	public void doDescribe(String objectName) throws ConnectionException {
		Map<String, String> describeMap;
		List<String> fieldNameSet = new Vector<String>();

		if (isConnected == false) {
			System.out.println("Not connected to Salesforce");
		} else {
			int maxLength = 0;

			Salesforce.QueryContext queryContext = salesforce.new QueryContext();

			queryContext.objectType = objectName;

			describeMap = salesforce.describeEntity(queryContext);

			fieldNameSet.addAll(describeMap.keySet());

			Collections.sort(fieldNameSet);

			for (String fieldName : fieldNameSet) {
				if (fieldName.length() > maxLength) {
					maxLength = fieldName.length();
				}
			}

			System.out.println("Object Name: " + objectName);
			System.out.println("");

			maxLength++;

			for (String fieldName : fieldNameSet) {
				String fieldType = describeMap.get(fieldName);

				System.out.println("\t" + String.format("%1$" + maxLength + "s", fieldName) + ": " + fieldType);
			}
		}
	}
	
	public String doQuery(String query) {
		String resultQuery = null;

		if (isConnected == false) {
			ConfirmationDialog.showDialog("Warning", "Not connected to Salesforce",
					"Before making a query you need to login on a Salesforce.com Instance", "ok", null);
		} else {
			asyncQuery = new SalesforceAsyncQuery(this);
			
			executeQueryProgressDialog = ProgressDialog.getDialog(
					asyncQuery,
					this.stage,
					Modality.WINDOW_MODAL,
					"Salesforce Query",
					"Query in progress, please wait",
					false, null, true, "cancel"
			);
			
			executeQueryProgressDialog.getStage().show();
			
			asyncQuery.run();
		}

		return resultQuery;
	}

	/**
	 * Convert a Java Object to String to be visible by end-user.
	 */
	public String convertObjectToString(Object obj) {
		String result = null;

		if (obj == null) {
			result = "";
		} else {
			if (obj instanceof Date) {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");

				result = simpleDateFormat.format((Date) obj);
			} else {
				result = String.valueOf(obj);
			}
		}

		return result;
	}

	/**
	 * Get the ordered list of Query Columns.
	 * 
	 * @param query
	 * @return
	 */
	private List<String> getOutputFieldList(String query) {
		List<String> fieldList = new Vector<String>();

		String patternSelectFieldsString = "^SELECT\\s+(.+)\\s+FROM\\s+([^\\s]+)";
		Pattern patternSelectFields = Pattern.compile(patternSelectFieldsString,
				Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

		Matcher matchSelectFields;

		query.replace("\r", " ").replace("\n", " ").replace("\t", " ");
		matchSelectFields = patternSelectFields.matcher(query);

		if (matchSelectFields.find()) {
			String selectFields = matchSelectFields.group(1);
			String salesforceEntity = matchSelectFields.group(2);
			String[] tmpFields = selectFields.split(",", -1);
			String[] fields = new String[tmpFields.length];

			for (int i = 0; i < tmpFields.length; i++) {
				fields[i] = tmpFields[i].trim();
			}

			for (String field : fields) {
				fieldList.add(field);
			}
		}

		return fieldList;
	}

	/**
	 * Read the content of file to String.
	 * 
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	private String getFileContent(String filename) throws IOException {
		System.out.println("InputFilename: \'" + filename + "\'");

		FileInputStream fileInputStream = new FileInputStream(filename);
		StringBuffer fileContent = new StringBuffer("");
		byte[] buffer = new byte[4096];
		int length;

		do {
			length = fileInputStream.read(buffer);

			if (length != -1) {
				fileContent.append(new String(buffer, "ISO-8859-1"));
			}
		} while (length != -1);

		fileInputStream.close();

		return fileContent.toString();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//Platform.setImplicitExit(false);
		
		primaryStage.setTitle(APPLICATION_TITLE);

		this.stage = primaryStage;

		try {
			setDockIcon();
		} catch (Throwable t) {
		}

		try {
			mainWindow(primaryStage);
		} catch (Throwable t) {
			ExceptionDialog.showDialog("Exception Caught", "An exception was caught in the application.",
					"Please see the Stack Trace dump for the Exception that was caught.", t, "exit");
		}
	}

	public void mainWindow(Stage stage) throws Throwable {
		Scene mainScene = null;
		VBox verticalBox = new VBox();
		Text textHeaderQuery = new Text("Text");
		Text textHeaderResult = new Text("Result");
		HBox horizontalBoxQueryButtons = new HBox();
		HBox horizontalBoxRunQueryButtons = new HBox();
		HBox horizontalBoxResultButtons = new HBox();

		// Initialize Styles
		textHeaderQuery.setStyle(TEXT_HEADER_STYLE);
		textHeaderResult.setStyle(TEXT_HEADER_STYLE);

		textAreaQuery.setPrefRowCount(INPUT_QUERY_ROWS);

		horizontalBoxRunQueryButtons.setAlignment(Pos.CENTER);

		// Query section
		horizontalBoxQueryButtons.getChildren().addAll(buttonAbout, buttonConnect, buttonLoad, buttonSave, buttonSaveAs,
				buttonDescribe, buttonNewBigObject, buttonEditBigObject);

		horizontalBoxRunQueryButtons.getChildren().addAll(buttonRunQuery);

		// Result section
		horizontalBoxResultButtons.getChildren().addAll(buttonExportAs);

		// compose the vertical layout
		verticalBox.setPadding(new Insets(5, 5, 5, 5));

		verticalBox.getChildren().addAll(horizontalBoxQueryButtons, textAreaQuery, horizontalBoxRunQueryButtons,
				tableView, horizontalBoxResultButtons);

		connectedTo.setFill(Color.RED);
		connectedTo.setTextAlignment(TextAlignment.CENTER);

		verticalBox.getChildren().add(connectedTo);

		tableView.getSelectionModel().setCellSelectionEnabled(true);

		MenuItem menuItem = new MenuItem("Copy");

		menuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				ObservableList<TablePosition> selectedCells = tableView.getSelectionModel().getSelectedCells();
				StringBuilder string = new StringBuilder();

				for (TablePosition selectedCell : selectedCells) {
					int row = selectedCell.getRow();
					int column = selectedCell.getColumn();
					Object cell = tableView.getColumns().get(column).getCellData(row);

					if (cell == null) {
						cell = "";
					}

					if (cell.toString() != null && cell.toString().trim().length() > 0) {
						string.append("\t");
					}

					string.append(cell);
				}

				// string.append("\n");

				final ClipboardContent content = new ClipboardContent();

				content.putString(string.toString().trim());

				Clipboard.getSystemClipboard().setContent(content);
			}
		});

		ContextMenu contextMenu = new ContextMenu();

		contextMenu.getItems().add(menuItem);
		tableView.setContextMenu(contextMenu);
		tableView.setMaxHeight(TABLEVIEW_MAX_HEIGHT);
		tableView.setPrefHeight(TABLEVIEW_MAX_HEIGHT);

		// Button Events
		buttonRunQuery.setOnAction((event) -> {
			try {
				String query = doQuery(textAreaQuery.getText());

				// textAreaQuery.setText(query);
			} catch (Throwable t) {
				t.printStackTrace();
			}
		});

		buttonLoad.setOnAction((event) -> {
			try {
				loadQuery();
			} catch (Throwable t) {
				t.printStackTrace();
			}
		});

		buttonSave.setOnAction((event) -> {
			try {
				saveQuery();
			} catch (Throwable t) {
				t.printStackTrace();
			}
		});

		buttonSaveAs.setOnAction((event) -> {
			try {
				saveAsQuery();
			} catch (Throwable t) {
				t.printStackTrace();
			}
		});

		buttonExportAs.setOnAction((event) -> {
			try {
				doExport();
			} catch (Throwable t) {
				t.printStackTrace();
			}
		});
		
		buttonAbout.setOnAction((event) -> { try { about(); } catch (Throwable t) { t.printStackTrace(); }});

		buttonConnect.setOnAction((event) -> {
			try {
				connect();
			} catch (Throwable t) {
				t.printStackTrace();
			}
		});

		buttonDescribe.setOnAction((event) -> {
			try {
				describe();
			} catch (Throwable t) {
				t.printStackTrace();
			}
		});

		buttonNewBigObject.setOnAction((event) -> {
			try {
				newBigObject();
			} catch (Throwable t) {
				t.printStackTrace();
			}
		});

		buttonEditBigObject.setOnAction((event) -> {
			try {
				editBigObject();
			} catch (Throwable t) {
				t.printStackTrace();
			}
		});

		mainScene = new Scene(verticalBox, MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);

		mainScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(final KeyEvent keyEvent) {
				KeyCodeCombination kcc = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_ANY);
				KeyCodeCombination kcc2 = new KeyCodeCombination(KeyCode.C, KeyCombination.META_ANY);

				if (kcc.match(keyEvent) || kcc2.match(keyEvent)) {
					ObservableList<TablePosition> selectedCells = tableView.getSelectionModel().getSelectedCells();
					StringBuilder string = new StringBuilder();

					for (TablePosition selectedCell : selectedCells) {
						int row = selectedCell.getRow();
						int column = selectedCell.getColumn();
						Object cell = tableView.getColumns().get(column).getCellData(row);

						if (cell == null) {
							cell = "";
						}

						if (cell.toString() != null && cell.toString().trim().length() > 0) {
							string.append("\t");
						}

						string.append(cell);
					}

					// string.append("\n");

					final ClipboardContent content = new ClipboardContent();

					content.putString(string.toString().trim());

					Clipboard.getSystemClipboard().setContent(content);
				}
			}
		});

		stage.setScene(mainScene);

		stage.getIcons().add(new Image(Application.class.getResourceAsStream(ICON_NAME)));

		stage.show();

		buttonSave.setDisable(true);
		buttonExportAs.setDisable(true);

		if (isConnected == false) {
			connect();
		}
	}

	public void about() throws Throwable {
		@SuppressWarnings("unused")
		AboutDialog aboutDialog = AboutDialog.show(stage);
	}
	
	public void connect() throws Throwable {
		SalesforceConnectionDialog dialog;

		Salesforce oldSalesforceConnection = salesforce;

		salesforce = null;

		dialog = SalesforceConnectionDialog.showConnectDialog(stage);

		if (dialog != null && dialog.isConnected()) {
			isConnected = true;

			salesforce = dialog.getConnection();
		} else {
			isConnected = false;
		}

		if (!isConnected && oldSalesforceConnection != null) {
			salesforce = oldSalesforceConnection;

			isConnected = true;
		}

		if (isConnected) {
			String connectedToText = "Connected as " + dialog.getUsername() + " to " + dialog.getConnectionName() + ".";
			
			connectedTo.setText(connectedToText);
		}
	}

	public void loadQuery() throws Throwable {
		FileChooser fileChooser = new FileChooser();
		File file;

		fileChooser.setTitle("Load a SOQL Query from file");

		fileChooser.getExtensionFilters().add(new ExtensionFilter("SOQL Files", "*.soql"));
		fileChooser.getExtensionFilters().add(new ExtensionFilter("SQL Files", "*.sql"));
		fileChooser.getExtensionFilters().add(new ExtensionFilter("All Files", "*.*"));

		file = fileChooser.showOpenDialog(stage);

		if (file != null) {
			FileReader fileReader = new FileReader(file);
			StringBuffer queryBuffer = new StringBuffer("");
			char[] buffer = new char[4096];

			while (fileReader.read(buffer) != -1) {
				queryBuffer.append(buffer);
			}

			fileReader.close();

			textAreaQuery.setText(queryBuffer.toString());
			openFilename = file.getAbsolutePath();
		}
	}

	public void saveQuery() throws Throwable {
		File file;

		file = new File(openFilename);

		if (file != null) {
			FileWriter fileWriter = new FileWriter(file);

			fileWriter.write(textAreaQuery.getText());

			fileWriter.close();
		}
	}

	public void saveAsQuery() throws Throwable {
		FileChooser fileChooser = new FileChooser();
		File file;

		fileChooser.setTitle("Save SOQL Query to file");

		fileChooser.getExtensionFilters().add(new ExtensionFilter("SOQL Files", "*.soql"));
		fileChooser.getExtensionFilters().add(new ExtensionFilter("SQL Files", "*.sql"));
		fileChooser.getExtensionFilters().add(new ExtensionFilter("All Files", "*.*"));

		file = fileChooser.showSaveDialog(stage);

		if (file != null) {
			FileWriter fileWriter = new FileWriter(file);

			fileWriter.write(textAreaQuery.getText());

			fileWriter.close();

			openFilename = file.getAbsolutePath();

			buttonSave.setDisable(false);
		}
	}

	public void doExport() throws IOException {
		FileChooser fileChooser = new FileChooser();
		File file;
		CSVWriter writer = null;
		int counter = 0;

		if (selectedFieldList != null && selectedFieldList.isEmpty() == false) {
			fileChooser.setTitle("Export Records to CSV file");

			fileChooser.getExtensionFilters().add(new ExtensionFilter("CSV Files", "*.csv"));
			fileChooser.getExtensionFilters().add(new ExtensionFilter("All Files", "*.*"));

			file = fileChooser.showSaveDialog(stage);

			if (file != null) {
				OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file, false));

				writer = new CSVWriter(outputStreamWriter, ',');

				writeCSV(writer, selectedFieldList);

				for (QueryResultRecord record : tableView.getItems()) {
					List<String> columns = new ArrayList<String>();
					counter++;

					for (String field : selectedFieldList) {
						String value = convertObjectToString(record.getValue(field));

						if (field.equalsIgnoreCase(ROW_NUM)) {
							value = String.valueOf(counter);
						}

						columns.add(value);
					}

					writeCSV(writer, columns);
				}

				outputStreamWriter.flush();

				writer.close();
			}
		}
	}

	/**
	 * Write CSV record.
	 * 
	 * @param writer
	 * @param columns
	 */
	public void writeCSV(CSVWriter writer, List<String> columns) {
		String[] entries = new String[columns.size()];

		for (int i = 0; i < columns.size(); i++) {
			entries[i] = columns.get(i);
		}

		writer.writeNext(entries);
	}

	public List<QueryResultRecord> getQueryResultRecordList(List<Map<String, Object>> recordList) {
		List<QueryResultRecord> queryResultList = new ArrayList<QueryResultRecord>();

		staticRowNum = 0;

		for (Map<String, Object> record : recordList) {
			QueryResultRecord queryResultRecord = new QueryResultRecord(record);

			//System.out.println(queryResultRecord);

			queryResultList.add(queryResultRecord);
		}

		return queryResultList;
	}

	public void describe() throws Throwable {
		List<FileProperties> customObjectList = salesforce.listMetadata("CustomObject", null);
		List<FilePropertiesComparable> customObjectList2 = new ArrayList<>();
		List<FileProperties> customObjectListSorted = new ArrayList<>();
		CustomObjectSelectDialog selectDialog;

		if (customObjectList != null) {
			for (FileProperties fp : customObjectList) {
				customObjectList2.add(new FilePropertiesComparable(fp));
			}

			Collections.sort(customObjectList2);

			for (FilePropertiesComparable fp : customObjectList2) {
				customObjectListSorted.add(fp.fp);
			}
		}

		selectDialog = CustomObjectSelectDialog.show(stage, customObjectListSorted);

		if (selectDialog.selectedEntry != null) {
			SalesforceDescribeDialog dialog = SalesforceDescribeDialog.showDescribeDialog(stage, salesforce,
					selectDialog.selectedEntry);
		}
	}

	public void newBigObject() throws Throwable {
		BigObjectDialog dialog = BigObjectDialog.show(stage, salesforce);
	}

	public void editBigObject() throws Throwable {
		List<FileProperties> customObjectList = salesforce.listBigObjects();
		List<FilePropertiesComparable> customObjectList2 = new ArrayList<>();
		List<FileProperties> customObjectListSorted = new ArrayList<>();

		if (customObjectList != null) {
			for (FileProperties fp : customObjectList) {
				customObjectList2.add(new FilePropertiesComparable(fp));
			}

			Collections.sort(customObjectList2);

			for (FilePropertiesComparable fp : customObjectList2) {
				customObjectListSorted.add(fp.fp);
			}
		}

		CustomObjectSelectDialog dialog = CustomObjectSelectDialog.show(stage, customObjectListSorted);

		if (dialog.selectedEntry != null) {
			BigObject bo = salesforce.fetchBigObject(dialog.selectedEntry);
			BigObjectDialog boDialog = BigObjectDialog.show(stage, salesforce, bo);
		}
	}

	public void setDockIcon() {
/*
		final JFrame jframe = new JFrame();
*/
		final Toolkit defaultToolkit = Toolkit.getDefaultToolkit();
		final URL imageResource = this.getClass().getResource("soqlplus3.png");
		final java.awt.Image image = defaultToolkit.getImage(imageResource);
		final Taskbar taskbar = Taskbar.getTaskbar();

		try {
			taskbar.setIconImage(image);
		} catch (Exception e) {
			System.out.println("Error setting Dock Icon: " + e.getMessage());
		}

/*
		jframe.setIconImage(image);
		jframe.getContentPane().add(new JLabel("Hello World"));
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.pack();
		jframe.setVisible(true);
*/

	}
}
