package com.app.soqlplus3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@SuppressWarnings("serial")
public class Connections extends Properties {
	public static final String propertiesFilename = initPropertiesFilename();
	
	public static final String CONNECTIONS_PROPERTIES_FILENAME = "soqlplus3.properties";
	public static final URL propertiesLocationURL = ClassLoader.getSystemResource(CONNECTIONS_PROPERTIES_FILENAME);
	
	public static String initPropertiesFilename() {
		String envHome = System.getenv("HOME");
		String result = "";
		
		if (Util.isEmpty(envHome)) {
			result = "." + CONNECTIONS_PROPERTIES_FILENAME;
		} else {
			result = envHome + File.separator + "." + CONNECTIONS_PROPERTIES_FILENAME;
		}
		
		System.out.println("propertiesFilename=" + result);
		
		if (Util.fileExists(result) == false) {
			try {
				Util.createFileWithContent(result, "## SOQLPlus3 Connections\n");
			} catch (Exception e) {
				System.out.println("Error fileCopy to " + result + ": " + Util.getStackTraceString(e));
				
				throw new RuntimeException("Error creating file " + result);
			}
		}
		
		return result;
	}
	
	public Connections() throws FileNotFoundException, IOException {
		reload();
	}
	
	public void reload() throws IOException {
		InputStream inputStream = new FileInputStream(propertiesFilename);
		
		load(inputStream);		
	}
	
	/**
	 * Get all the aliases stored on the sfdc-bigobject.properties file.
	 * 
	 * @return
	 */
	public List<String> getAliases() {
		Set<String> aliasSet = new HashSet<String>();
		List<String> aliasList = new ArrayList<String>();
		
		for (Object keyObject : keySet()) {
			String key = (String)keyObject;
			
			key = key.substring(0, key.indexOf("."));
			
			aliasSet.add(key);
		}
		
		aliasList.addAll(aliasSet);
		
		Collections.sort(aliasList);
		
		return aliasList;
	}
	/**
	 * Get a connection from the connections map indexed by connection name.
	 * @param connectionName
	 * @return
	 */
	public Map<String, String> getConnection(String connectionName) {
		Map<String,String> connectionMap = new HashMap<String,String>();
		
		//System.out.println("connecting to " + connectionName);

		for (Object keyObject : keySet()) {
			String keyName = (String)keyObject;
			
			//System.out.println("inspecting key " + keyName);
			
			if (keyName.startsWith(connectionName + ".")) {
				String attributeName = keyName.substring(connectionName.length() + 1);
				String attributeValue = (String)get(keyName);
			
				//System.out.println("returning key: " + attributeName + ", value: " + attributeValue);
				
				connectionMap.put(attributeName, attributeValue);
			}
		}
		
		return connectionMap;
	}
	
	public void setConnection(String connectionName, String loginServer, String username, String password, String securityToken) throws FileNotFoundException, IOException {
		put(connectionName + ".loginServer", loginServer != null ? loginServer : "");
		put(connectionName + ".username", username != null ? username : "");
		put(connectionName + ".password", password != null ? password : "");
		put(connectionName + ".securityToken", securityToken != null ? securityToken : "");
		
		store(new FileOutputStream(propertiesFilename), "");
	}
	
	public void deleteConnection(String connectionName) throws FileNotFoundException, IOException {
		Set<Object> keyObjectSet = keySet();
		List<String> removeKeyList = new ArrayList<String>();
		
		for (Object keyObject : keyObjectSet) {
			String keyName = (String)keyObject;
			
			if (keyName.startsWith(connectionName + ".")) {
				removeKeyList.add(keyName);
			}
		}
		
		for (String removeKey : removeKeyList) {
			remove(removeKey);
		}
		
		store(new FileOutputStream(propertiesFilename), "");
	}
	
	public void replaceConnection(String oldConnectionName, String connectionName, String loginServer, String username, String password, String securityToken) throws FileNotFoundException, IOException {
		Set<Object> keyObjectSet = keySet();
		List<String> removeKeyList = new ArrayList<String>();
		
		for (Object keyObject : keyObjectSet) {
			String keyName = (String)keyObject;
			
			if (keyName.startsWith(oldConnectionName + ".")) {
				removeKeyList.add(keyName);
			}
		}
		
		for (String removeKey : removeKeyList) {
			remove(removeKey);
		}
		
		put(connectionName + ".loginServer", loginServer != null ? loginServer : "");
		put(connectionName + ".username", username != null ? username : "");
		put(connectionName + ".password", password != null ? password : "");
		put(connectionName + ".securityToken", securityToken != null ? securityToken : "");
		
		store(new FileOutputStream(propertiesFilename), "");
	}
	
	@Override
	public void store(OutputStream out, String comments) throws IOException {
		List<String> keyList = new ArrayList<String>();
		String lastKeyPrefix = null;
		
		
		for (Object keyObject : keySet()) {
			keyList.add((String)keyObject);
		}
		
		Collections.sort(keyList);

		if (comments != null) {
			out.write(("#" + comments + "\n").getBytes());
		}
		
		for (String key : keyList) {
			String keyPrefix = key.substring(0, key.lastIndexOf("."));
			String value = (String)get(key);
			
			if (lastKeyPrefix != null && ! lastKeyPrefix.equals(keyPrefix)) {
				System.out.println("saving >> \n");

				out.write("\n".getBytes());
			}
			
			System.out.println("saving >> " + key + "=" + value + "\n");

			out.write((key + "=" + value + "\n").getBytes());
				
			lastKeyPrefix = keyPrefix;
		}

		out.flush();
		out.close();
	}
}
