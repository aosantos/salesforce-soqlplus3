package com.app.soqlplus3;

import com.sforce.soap.metadata.AsyncResult;
import com.sforce.soap.metadata.CodeCoverageWarning;
import com.sforce.soap.metadata.DeployDetails;
import com.sforce.soap.metadata.DeployMessage;
import com.sforce.soap.metadata.DeployOptions;
import com.sforce.soap.metadata.DeployResult;
import com.sforce.soap.metadata.FileProperties;
import com.sforce.soap.metadata.ListMetadataQuery;
import com.sforce.soap.metadata.MetadataConnection;
import com.sforce.soap.metadata.PackageTypeMembers;
import com.sforce.soap.metadata.RetrieveMessage;
import com.sforce.soap.metadata.RetrieveRequest;
import com.sforce.soap.metadata.RetrieveResult;
import com.sforce.soap.metadata.RunTestFailure;
import com.sforce.soap.metadata.RunTestsResult;
import com.sforce.soap.partner.Connector;
import com.sforce.soap.partner.DescribeSObjectResult;
import com.sforce.soap.partner.Field;
import com.sforce.soap.partner.FieldType;
import com.sforce.soap.partner.LoginResult;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;
import com.sforce.ws.SoapFaultException;
import com.sforce.ws.bind.XmlObject;

import com.app.soqlplus3.dialog.ExceptionDialog;
import com.app.soqlplus3.model.BigObject;
import com.app.soqlplus3.model.BigObjectCustomField;
import com.app.soqlplus3.model.Index;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

//import com.sforce.soap.metadata.MetadataConnection;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Salesforce {
	private static final String QUERY_ORGANIZATION = "SELECT Id, Name FROM Organization";
	
	public static final String SFDC_API_VERSION = "42.0";
	
	private static final double METADATA_API_VERSION = Double.valueOf(SFDC_API_VERSION);
	
	public static final int MAX_NUM_POLL_REQUESTS = 50;
	
	private static final String MANIFEST_FILE = "package.xml";

	private static final long ONE_SECOND = 1000;
	
	final String envDebug = System.getenv("DEBUG");
	
	public Boolean allOrNone = false;
	
	private ConnectorConfig partnerConfig;
	public PartnerConnection partnerConnection;
	private MetadataConnection metadataConnection;
	
	private LoginResult loginResult;
	
	private String username;
	private String password;
	
	public Boolean isConnected;

	public class FieldDescribe {
		public String name;
		public String label;
		public String type;
		public String extraTypeInfo;
		public String soapType;
		public String calculatedFormula;
		public Boolean isNillable;
		public Boolean isAutoNumber;
		public Boolean isCalculated;
		public Boolean isCaseSensitive;
		public Boolean isCreate;
		public Boolean isCustom;
		public Boolean isEncrypted;
		public Boolean isExternalId;
		public Boolean isDeprecatedAndHidden;
		public Integer byteLength;
		public Integer digits;
		public Integer length;
		public Integer precision;
		public List<String> referenceTo;
		
		public FieldDescribe() {
			referenceTo = new ArrayList<>();
		}
		
		public String salesforceType() {
			if ("id".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "Id" + (isCalculated ? ")" : "");
			} else if ("_boolean".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "Checkbox" + (isCalculated ? ")" : "");
			} else if ("string".equals(type)) { 
				return (isCalculated ? "Formula(" : "") + "Text(" + length + ")" + (
						isExternalId ?
							"(external id)" + "(case " + (isCaseSensitive ? "Sensitive" : "insensitive") + ")"
							: ""
				) + (isCalculated ? ")" : "");
			} else if ("reference".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "Lookup(" + String.join(",", referenceTo) + ")" + (isCalculated ? ")" : "");
			} else if ("email".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "Email" + (isCalculated ? ")" : "");
			} else if ("picklist".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "Picklist" + (isCalculated ? ")" : "");
			} else if ("multipicklist".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "Multi-Select Picklist" + (isCalculated ? ")" : "");
			} else if ("textarea".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "TextArea(" + length + ")" + (isCalculated ? ")" : "");
			} else if ("date".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "Date" + (isCalculated ? ")" : "");
			} else if ("location".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "Location" + (isCalculated ? ")" : "");
			} else if ("datetime".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "DateTime" + (isCalculated ? ")" : "");
			} else if ("_double".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "Number(" + precision + "," + digits + ")" + (isCalculated ? ")" : "");
			} else if ("_int".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "Number(" + digits + ")" + (isCalculated ? ")" : "");
			} else if ("percent".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "Percentage(" + precision + "," + digits + ")" + (isCalculated ? ")" : "");
			} else if ("currency".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "Currency(" + precision + "," + digits + ")" + (isCalculated ? ")" : "");
			} else if ("phone".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "Phone" + (isCalculated ? ")" : "");
			} else if ("address".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "Address" + (isCalculated ? ")" : "");
			} else if ("url".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "URL" + (isCalculated ? ")" : "");
			} else if ("combobox".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "ComboBox" + (isCalculated ? ")" : "");
			} else if ("base64".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "Base64" + (isCalculated ? ")" : "");
			} else if ("anyType".equals(type)) {
				return (isCalculated ? "Formula(" : "") + "AnyType" + (isCalculated ? ")" : "");
			} else {
				return "unknown type [" + type + "]...";
			}
		}
	}
	
	public class ObjectDescribe {
		public String objectName;
		public String objectLabel;
		
		public List<FieldDescribe> fieldList;
		
		public ObjectDescribe() {
			fieldList = new ArrayList<>();
		}
	}
	
	QueryContext lastQueryContext;
	
	public class QueryContext {
		public String objectType;
		public List<String> fieldList;
		public Map<String,FieldType> fieldTypeMap;
		public Map<String,String> fieldReferenceMap;
		public Map<String,String> caseSensitiveFieldNameMap;
		public String query;
		public String whereClause;
		public Integer countRecords;
		public Map<String,FieldType> describeFieldTypeMap;
		
		public QueryContext() {
			fieldList = new Vector<String>();
			fieldTypeMap = new HashMap<String,FieldType>();
			caseSensitiveFieldNameMap = new HashMap<String,String>();
			fieldReferenceMap = new HashMap<String,String>();
			describeFieldTypeMap = new HashMap<String,FieldType>();
		}
		
		public String toString() {
			String stringFieldType = "";
			String stringFieldTypeMap = "";
			String stringFieldReferenceMap = "";
			int i;
			
			for (i = 0; i < fieldList.size(); i++) {
				if (i > 0) {
					stringFieldType += ",\n";
				}
				stringFieldType += fieldList.get(i);
			}
			
			i = 0;
			for (String fieldName : fieldTypeMap.keySet()) {
				if (i > 0) {
					stringFieldTypeMap += ",\n";
				}
				stringFieldTypeMap += fieldName + ": " + fieldTypeMap.get(fieldName);
				i++;
			}

			i = 0;
			for (String fieldName : fieldReferenceMap.keySet()) {
				if (i > 0) {
					stringFieldReferenceMap += ",\n";
				}
				stringFieldReferenceMap += fieldName + ": " + fieldReferenceMap.get(fieldName);
				i++;
			}
			
			return "objectType: " + objectType + ", fieldList: [ " + stringFieldType + " ]" + ", fieldTypeMap: { " + stringFieldTypeMap + " }, fieldReferenceMap: { " + stringFieldReferenceMap + " }";
		}
	}
	
	public Salesforce() {
	}
	
	/**
	 * Connect to Salesforce.
	 * 
	 * @param loginServer
	 * @param username
	 * @param password
	 * @param securityToken
	 * @return
	 * @throws ConnectionException
	 */
	public Salesforce getConnection(String loginServer, String username, String password, String securityToken) throws ConnectionException {
		partnerConfig = new ConnectorConfig();
		
		partnerConfig.setManualLogin(true);
		partnerConfig.setServiceEndpoint(loginServer);
		partnerConfig.setAuthEndpoint(loginServer);
		partnerConfig.setValidateSchema(false);
		
		this.username = username;
		this.password = password + securityToken;
		
		initializePartnerConnection();
		initializeMetadataConnection();
		
		checkIfConnected();
		
		return this;
	}
	
	public MetadataConnection getMetadataConnection() throws ConnectionException {
		return metadataConnection;
	}
	
	private void initializeMetadataConnection() throws ConnectionException {
		ConnectorConfig metadataConfig = new ConnectorConfig();
		
		System.out.println("sessionId=" + loginResult.getSessionId());
		System.out.println("metadataServerUrl=" + loginResult.getMetadataServerUrl());
		
		metadataConfig.setSessionId(loginResult.getSessionId());
		metadataConfig.setServiceEndpoint(loginResult.getMetadataServerUrl());
		
		metadataConnection = new MetadataConnection(metadataConfig);
	}
	
	/**
	 * Normalize a SOQL Query, remove enters, extra spacing, etc.
	 * 
	 * @param query
	 * @return
	 */
	public String normalizeSoqlQuery(String query) {
		String normalizedQuery = query.replace("\n", "").replace("\t", " ").replaceAll("\\s+", " ");
				
		return normalizedQuery;
	}
	
	/**
	 * Execute a SOQL Query and return a List of Records where each element
	 * is a Map with key set as the column name and value set as the object
	 * fetched for the corresponding column.
	 * 
	 * @param soqlQuery
	 * @return
	 * @throws ConnectionException 
	 * @throws ParseException 
	 */
	public List<Map<String,Object>> executeQuery(String soqlQuery) throws ConnectionException, ParseException {
		List<Map<String,Object>> recordList = new Vector<Map<String,Object>>();
		QueryContext queryContext = new QueryContext();
		int iteration = 0;
		int totalRecords = 0;
		
		queryContext.query = normalizeSoqlQuery(soqlQuery);
		parseQueryMedata(soqlQuery, queryContext);
		
		System.out.println("Normalized Query:\n\t" + queryContext.query + "\n"); 
		
		QueryResult queryResult = partnerConnection.query(queryContext.query);
		
		if (queryResult.getSize() > 0) {
			do {
				SObject[] records;
				
				if (iteration > 0) {
					queryResult = partnerConnection.queryMore(queryResult.getQueryLocator());
				}

				records = queryResult.getRecords();
				
				if (records != null && records.length > 0) {
					totalRecords += records.length;
					
					for (int recordNo = 0; recordNo < records.length; recordNo++) {
						SObject record = records[recordNo];
						Map<String,Object> recordFields = new HashMap<String,Object>();
						
						for (int fieldNo = 0; fieldNo < queryContext.fieldList.size(); fieldNo++) {
							String fieldName;
							Object fieldValue;
							
							fieldName = queryContext.fieldList.get(fieldNo);
							fieldValue = getFieldValue(queryContext, record, fieldNo);
							
							//System.out.println("FieldName: " + fieldName + ", FieldValue: " + fieldValue.toString() + ", FieldType: " + fieldValue.getClass().getName());
							
							/*
							if (
								envDebug != null
								&&
								(
										envDebug.trim().equalsIgnoreCase("true")
										||
										envDebug.trim().equalsIgnoreCase("yes")
										||
										! envDebug.trim().equalsIgnoreCase("0")
								)
							) {
								System.out.println("Record " + (recordNo+1) + ", Field '" + fieldName + "', Value '" + fieldValue + "'" + "\n" + record.toString());
							}
							*/
							
							recordFields.put(fieldName, fieldValue);
						}
						
						recordList.add(recordFields);
					}
				}
				
				iteration++;
			} while (queryResult.isDone() == false);
		}
		
		lastQueryContext = queryContext;
		
		return recordList;
	}
	
	/**
	 * Get the last query executed query context.
	 * 
	 * @return
	 */
	public QueryContext getLastQueryContext() {
		return lastQueryContext;
	}
	/**
	 * Initialize the Partner Connection.
	 * 
	 * @throws ConnectionException
	 */
	private void initializePartnerConnection() throws ConnectionException {
		partnerConnection = Connector.newConnection(partnerConfig);
		
		loginResult = partnerConnection.login(username, password);
		
		if (loginResult.isPasswordExpired()) {
			throw new RuntimeException("Salesforce Login: Password Expired");
		}
		
		partnerConnection.setSessionHeader(loginResult.getSessionId());
		partnerConnection.setAllOrNoneHeader(allOrNone);
		
		partnerConfig.setServiceEndpoint(loginResult.getServerUrl());
	}
	
	/**
	 * Check if Connected to Salesforce, using a sample Query.
	 * 
	 * @throws ConnectionException
	 */
	private void checkIfConnected() throws ConnectionException {
		QueryResult queryResult;
		Boolean failure = true;
		
		isConnected = false;
		queryResult = partnerConnection.query(QUERY_ORGANIZATION);
		
		if (queryResult.getSize() > 0) {
			SObject[] records = queryResult.getRecords();
			
			for (int i = 0; i < records.length; i++) {
				String id;
				String name;
				
				SObject record = records[i];
				
				id = (String)record.getSObjectField("Id");
				name = (String)record.getSObjectField("Name");
				
				if (id != null && name != null) {
					failure = false;
					break;
				}
			}
		}
		
		if (failure) {
			throw new RuntimeException("Error making connectivity test to Salesforce.");
		} else {
			isConnected = true;
		}
	}
	
	/**
	 * Parse the select fields into select items.
	 * 
	 * @param selectFields
	 * @return
	 * @throws ConnectionException 
	 */
	private String[] getFields(String objectName, String selectFields) throws ConnectionException {
		String[] result;
		
		if (selectFields.equals("*")) {
			ObjectDescribe describeResult = describeObject(objectName);
			List<FieldDescribe> fieldDescribeList = describeResult.fieldList;
			int fieldIndex = -1;
			int totalFields = 0;

			// discount for address fields
			for (FieldDescribe fd : fieldDescribeList) {
				if (fd.type.equals("address") == false) {
					totalFields++;
				}
			}
			
			result = new String[totalFields];
			
			for (FieldDescribe fieldDescribe : fieldDescribeList) {
				if (fieldDescribe.type.equals("address") == false) {
					fieldIndex++;
					
					result[fieldIndex] = fieldDescribe.name;
					
					//System.out.println("FieldIndex: " + fieldIndex + ", Name: " + fieldDescribe.name + ", Type: " + fieldDescribe.type);
				}
			}
		} else {
			result = selectFields.split(",", -1);
		}
		
		return result;
	}
	
	/**
	 * Parse Query Metadata.
	 * 
	 * @param query
	 * @throws ConnectionException 
	 */
	public void parseQueryMedata(String query, Salesforce.QueryContext queryContext) throws ConnectionException {
		String normalizedQuery = normalizeQuery(query);
		String patternSelectFieldsString = "^SELECT\\s+(.+)\\s+FROM\\s+([^\\s]+)(\\s+(.*))?$";
		Pattern patternSelectFields = Pattern.compile(patternSelectFieldsString, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
		Matcher matchSelectFields = patternSelectFields.matcher(normalizedQuery);
		
		if (matchSelectFields.find()) {
			String selectFields = matchSelectFields.group(1);
			String salesforceEntity = matchSelectFields.group(2);
			String where = matchSelectFields.group(3);
			String[] tmpFields = getFields(salesforceEntity, selectFields);
			String[] fields = new String[tmpFields.length];
			
			if (where != null && where.trim().isEmpty()) {
				where = null;
			}
			
			for (int i = 0; i < tmpFields.length; i++) {
				fields[i] = tmpFields[i].trim().toUpperCase();
			}
			
			queryContext.objectType = salesforceEntity;
			queryContext.whereClause = parseWhereClause(where);
			
			for (String field : fields) {
				queryContext.fieldList.add(field);
			}
			
			describeFields(queryContext);
			describeEntity(queryContext);
			
			queryContext.query = "SELECT " + String.join(", ", fields) + " FROM " + salesforceEntity + (where != null ? " " + where : "");
			
			if (!salesforceEntity.endsWith("__b")) {
				getRecordCount(queryContext);
			}
		}
	}
	
	private String parseWhereClause(String clause) {
		String result = "";
		
		if (clause != null && clause.trim().isEmpty() == false) {
			String wherePatternExpression = "\\s*(WHERE.*)(ORDER|LIMIT)\\s.*?$";
			Pattern wherePattern = Pattern.compile(wherePatternExpression, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
			Matcher whereMatch = wherePattern.matcher(clause);
			
			if (whereMatch.find()) {
				String whereClause = whereMatch.group(1);
				
				result = whereClause;
			}
		}
		
		return result;
	}
	
	private void getRecordCount(QueryContext queryContext) throws ConnectionException {
		String countQuery = "SELECT COUNT(Id) totalCount FROM " + queryContext.objectType + (queryContext.whereClause != null ? " " + queryContext.whereClause : "");
		QueryResult queryResult;
		
		System.out.println("counting records: " + countQuery);
		
		queryResult = partnerConnection.query(countQuery);
		
		queryContext.countRecords = 0;
		
		if (queryResult != null) {
			SObject[] resultList = queryResult.getRecords();
			
			if (resultList != null && resultList.length > 0) {
				SObject result = resultList[0];
				Integer totalCount = (Integer)result.getField("totalCount");
				
				queryContext.countRecords = totalCount.intValue();
			}
		}
		
		System.out.println("total records: " + queryContext.countRecords);
	}
	
	
	/**
	 * Normalize the SOQL Query.
	 * 
	 * @param query
	 * @return
	 */
	private String normalizeQuery(String query) {
		 String result = "";
		 
		 result = query.replace("\r",  " ").replace("\n",  " ").replace("\t", " ");
		 
		 return result;
	}
	
	private void describeFields(QueryContext queryContext) throws ConnectionException {
		DescribeSObjectResult describeObject;
		Field[] fieldDescribe;
		Map<String,Map<String,FieldType>> typeMap = new HashMap<String,Map<String,FieldType>>();
		
		queryContext.fieldReferenceMap.put("", queryContext.objectType);
		queryContext.fieldReferenceMap.put(queryContext.objectType, queryContext.objectType);
		
		for (String field : queryContext.fieldList) {
			String fieldAddress = extractFieldAddress(field);
			String fieldName = extractFieldName(field).toUpperCase();
			String baseEntity;
			Map<String,FieldType> fieldTypeByName = new HashMap<String,FieldType>();
			
			if (queryContext.fieldReferenceMap.containsKey(fieldAddress) == false) {
				buildAddressReference(queryContext, queryContext.objectType, fieldAddress);
			}
			
			baseEntity = queryContext.fieldReferenceMap.get(fieldAddress);
			
			//System.out.println("BaseEntity: " + baseEntity);
			
			if (typeMap.containsKey(baseEntity)) {
				if (fieldTypeByName.containsKey(fieldName)) {
					FieldType fieldType = fieldTypeByName.get(fieldName);
					
					//System.out.println("FieldTypeMap: " + field.toUpperCase());
					
					queryContext.fieldTypeMap.put(field.toUpperCase(), fieldType);
				}
			} else {
				describeObject = partnerConnection.describeSObject(baseEntity);
				fieldDescribe = describeObject.getFields();
				
				typeMap.put(baseEntity, fieldTypeByName);
				
				for (int i = 0; i < fieldDescribe.length; i++) {
					String caseSensitiveFieldNameKey;
					Field fieldDefinition = fieldDescribe[i];
					String fname = fieldDefinition.getName().toUpperCase();
					FieldType fieldType = fieldDefinition.getType();
					
					caseSensitiveFieldNameKey = (baseEntity + "." + fname).toUpperCase();
					queryContext.caseSensitiveFieldNameMap.put(caseSensitiveFieldNameKey, fieldDefinition.getName());
					
					fieldTypeByName.put(fname, fieldType);
					
					queryContext.fieldTypeMap.put(fname, fieldType);
					
					//System.out.println("BaseEntity: " + baseEntity + ", fieldName: " + fname + ", FieldType: " + fieldDefinition.getType());
				}
				
				if (fieldTypeByName.containsKey(fieldName)) {
					FieldType fieldType = fieldTypeByName.get(fieldName);
					
					//System.out.println("FieldTypeMap: " + field.toUpperCase());
					
					queryContext.fieldTypeMap.put(field.toUpperCase(), fieldType);
				}
			}
		}
	}
	
	public Map<String,String> describeEntity(QueryContext queryContext) throws ConnectionException {
		DescribeSObjectResult describeObject = partnerConnection.describeSObject(queryContext.objectType);
		Field[] fields = describeObject.getFields();
		Map<String,String> describeResult = new HashMap<String,String>();
		
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			String fieldName = field.getName().toUpperCase();
			FieldType fieldType = field.getType();
			
			//System.out.println("FieldTypeMap: " + fieldName);
			//System.out.println("FieldTypeMap: " + queryContext.objectType + "." + fieldName);
			
			queryContext.fieldTypeMap.put(fieldName, fieldType);
			queryContext.fieldTypeMap.put(queryContext.objectType + "." + fieldName, fieldType);
			
			queryContext.describeFieldTypeMap.put(field.getName(), field.getType());
			
			describeResult.put(field.getName(), field.getType().name());
		}
		
		return describeResult;
	}
	
	private String extractFieldAddress(String field) {
		String address = null;
		int lastIndexOfDot = field.lastIndexOf(".");
		
		if (lastIndexOfDot > 0) {
			address = field.substring(0, lastIndexOfDot);
		} else {
			address = "";
		}
		
		return address;
	}
	
	private String extractFieldName(String field) {
		String fieldName = null;
		int lastIndexOfDot = field.lastIndexOf(".");
		
		if (lastIndexOfDot > 0) {
			fieldName = field.substring(lastIndexOfDot + 1);
		} else {
			fieldName = field;
		}
		
		return fieldName;
	}
	
	private void buildAddressReference(QueryContext queryContext, String baseEntity, String address) throws ConnectionException {
		for (String prefixAddress : walkAddress(baseEntity, address, false)) {
			String baddr = extractFieldAddress(prefixAddress);
			String faddr = extractFieldName(prefixAddress);
			String describeEntity = null;
			String describeField = null;
			String fieldEntity = null;
			
			if (queryContext.fieldReferenceMap.containsKey(baddr)) {
				describeEntity = queryContext.fieldReferenceMap.get(baddr);
				describeField = faddr;
				
				if (describeField.endsWith("__r") || describeField.endsWith("__R")) {
					describeField = describeField.replaceAll("__r$", "__c");
					describeField = describeField.replaceAll("__R$", "__c");
				}
				
				fieldEntity = getEntityReferenceType(describeEntity, describeField);
				
				queryContext.fieldReferenceMap.put(prefixAddress, fieldEntity);
			}
		}
	}
	
	private List<String> walkAddress(String baseEntity, String address, Boolean dontAppend) {
		String[] items = address.split("\\.");
		List<String> walk = new Vector<String>();
		int startIndex = 0;
		
		if (address.startsWith(baseEntity)) {
			startIndex = 1;
		}
		
		for (int i = startIndex; i < items.length; i++) {
			String item = "";
			
			for (int j = 0; j <= i; j++) {
				if (dontAppend == false) {
					if (j > 0) {
						item += ".";
					}
					
					item += items[j];
				} else {
					item = items[j];
				}
			}
			
			walk.add(item);
		}
		
		return walk;
	}
	
	private String getEntityReferenceType(String objectType, String fieldName) throws ConnectionException {
		DescribeSObjectResult describeObject = partnerConnection.describeSObject(objectType);
		Field[] objectFields = describeObject.getFields();
		
		for (int i = 0; i < objectFields.length; i++) {
			Field field = objectFields[i];
			FieldType fieldType = field.getType();
			
			if (fieldName.equalsIgnoreCase(field.getName()) || (fieldName + "Id").equalsIgnoreCase(field.getName())) {
				if (fieldType == FieldType.reference) {
					String[] referenceToList = field.getReferenceTo();
					
					if (referenceToList != null && referenceToList.length > 0) {
						return referenceToList[0];
					}
				}
			}
		}
		
		throw new RuntimeException("Error getting entity reference type for object " + objectType + " on field " + fieldName);
	}
	
	public Object getFieldValue(QueryContext queryContext, SObject sobject, int fieldNo) throws ParseException {
		Object result = null;
		String objectType = queryContext.objectType;
		String fullFieldName = queryContext.fieldList.get(fieldNo);
		FieldType fieldType = queryContext.fieldTypeMap.get(fullFieldName.toUpperCase());
		Object fieldValue;
		String fieldName;
		
		//System.out.println("fieldTypeMapKey indexed: " + fullFieldName);
		
		fieldName = extractFieldName(fullFieldName);
		
		if (fieldType == null) {
			//System.out.println("fieldTypeMapKey indexed: " + fieldName.toUpperCase());
			
			fieldType = queryContext.fieldTypeMap.get(fieldName.toUpperCase());
		}
		
		if (fullFieldName.contains(".")) {
			fieldValue = getSObjectFieldReference(objectType, sobject, fullFieldName); 
		} else {
			String caseSensitiveFieldNameKey = (objectType + "." + fieldName).toUpperCase();
			String caseSensitiveFieldName = queryContext.caseSensitiveFieldNameMap.get(caseSensitiveFieldNameKey);
			
			if (caseSensitiveFieldName == null) {
				caseSensitiveFieldName = fieldName;
			}
			
			try {
				fieldValue = sobject.getSObjectField(caseSensitiveFieldName);
			} catch (Throwable t) {
				ExceptionDialog.showDialog("Exception Caught", "An error was found when executing the query", "The field: " + caseSensitiveFieldName + " could not be fetched from the resultset: " + Util.getStackTraceString(t), t, "ok");
				
				throw t;
			}
		}
		
		//System.out.println("fieldNo: " + fieldNo + ", fieldValue: " + fieldValue + ", fieldType: " + fieldType);
		
		/*
		if (fieldType == null) {
			System.out.println("fullFieldName: " + fullFieldName + " field type is NULL");
		}
		*/
		
		result = getNewObjectInstance(fieldType, fieldValue);
		
		return result;
	}
	
	private Object getNewObjectInstance(FieldType fieldType, Object fieldValue) throws ParseException {
		Object result = null;
		
		Set<String> booleanTypeSet = new HashSet<String>();
		Set<String> decimalTypeSet = new HashSet<String>();
		Set<String> integerTypeSet = new HashSet<String>();
		Set<String> dateTypeSet = new HashSet<String>();
		Set<String> datetimeTypeSet = new HashSet<String>();
		Set<String> stringTypeSet = new HashSet<String>();
		
		booleanTypeSet.add(FieldType._boolean.toString());
		
		decimalTypeSet.add(FieldType._double.toString());
		decimalTypeSet.add(FieldType.currency.toString());
		decimalTypeSet.add(FieldType.currency.toString());
		decimalTypeSet.add(FieldType.percent.toString());
		
		integerTypeSet.add(FieldType._int.toString());
		
		dateTypeSet.add(FieldType.date.toString());
		
		datetimeTypeSet.add(FieldType.datetime.toString());
		
		stringTypeSet.add(FieldType.combobox.toString());
		stringTypeSet.add(FieldType.email.toString());
		stringTypeSet.add(FieldType.id.toString());
		stringTypeSet.add(FieldType.multipicklist.toString());
		stringTypeSet.add(FieldType.phone.toString());
		stringTypeSet.add(FieldType.picklist.toString());
		stringTypeSet.add(FieldType.reference.toString());
		stringTypeSet.add(FieldType.string.toString());
		stringTypeSet.add(FieldType.url.toString());
		stringTypeSet.add(FieldType.encryptedstring.toString());
		stringTypeSet.add(FieldType.textarea.toString());
		
		if (fieldType != null) {
			//System.out.println("fieldType: " + fieldType.toString());
		}
		
		if (fieldType == null) {
			result = null;
		} else {
			if (booleanTypeSet.contains(fieldType.toString())) {
				String resultFieldValue = null;
				
				//System.out.println(fieldValue);
				
				if (fieldValue != null) {
					resultFieldValue = String.valueOf(fieldValue);
					
				}
				
				result = fieldValue == null ? false : (resultFieldValue.equalsIgnoreCase("true") ? true : false);
			} else if (dateTypeSet.contains(fieldType.toString())) {
				String resultFieldValue = null;
				
				//System.out.println(fieldValue);
				
				if (fieldValue != null) {
					resultFieldValue = String.valueOf(fieldValue);
				}
				
				if (resultFieldValue != null) {
					SimpleDateFormat dtFormat2 = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
					Calendar currentCalendarDate = Calendar.getInstance();
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
					String simpleDateFormatInput = resultFieldValue;
					Calendar fieldCalendarDate = Calendar.getInstance();
					
					try {
						fieldCalendarDate.setTime(simpleDateFormat.parse(simpleDateFormatInput));
					} catch (ParseException e) {
						fieldCalendarDate.setTime(dtFormat2.parse(simpleDateFormatInput));
					}
					
					fieldCalendarDate.add(Calendar.MILLISECOND, currentCalendarDate.getTimeZone().getDSTSavings());
					
					result = fieldCalendarDate.getTime();
				}
			} else if (datetimeTypeSet.contains(fieldType.toString())) {
				String resultFieldValue = null;
				
				if (fieldValue != null) {
					resultFieldValue = (String)fieldValue;
				}
				
				if (resultFieldValue != null) {
					Calendar currentCalendarDate = Calendar.getInstance();
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
					String simpleDateFormatInput = resultFieldValue.replace("T", " ").replace("Z", "+000");
					Calendar fieldCalendarDate = Calendar.getInstance();
					
					fieldCalendarDate.setTime(simpleDateFormat.parse(simpleDateFormatInput));
					fieldCalendarDate.add(Calendar.MILLISECOND, currentCalendarDate.getTimeZone().getDSTSavings());
					
					result = fieldCalendarDate.getTime();
				}
			} else if (decimalTypeSet.contains(fieldType.toString())) {
				String resultFieldValue = null;
				
				if (fieldValue != null) {
					resultFieldValue = (String)fieldValue;
				}
				
				if (resultFieldValue != null) {
					result = Double.valueOf(resultFieldValue);
				}
			} else if (integerTypeSet.contains(fieldType.toString())) {
				String resultFieldValue = null;
				
				if (fieldValue != null) {
					resultFieldValue = (String)fieldValue;
				}
				
				if (resultFieldValue != null) {
					result = Integer.valueOf(resultFieldValue);
				}
			} else if (stringTypeSet.contains(fieldType.toString())) {
				result = (String)fieldValue;
			}
		}
		
		return result;
	}
	
	private Object getSObjectFieldReference(String objectType, SObject sobject, String field) {
		List<String> path;
		Object result = null;
		SObject currentRecord;
		int pathIndex;
		String currentPathItem = "";
		
		path = walkAddress(objectType, field, true);
		
		pathIndex = 0;
		currentRecord = sobject;
		for (String pathItem : path) {
			if (pathIndex > 0) {
				currentPathItem += ".";
			}
			currentPathItem += pathItem;
			
			if (path.size() > 1 && pathIndex < path.size() - 1 && (pathItem.endsWith("__c") || pathItem.endsWith("__C")) == false) {
				if (currentRecord != null) {
					Map<String,String> sobjectFieldMap = new HashMap<>();
					Iterator<XmlObject> iter = currentRecord.getChildren();
					String pathItemCaseSensitive;
					
					while (iter.hasNext()) {
						XmlObject xmlObject = iter.next();
						String key = xmlObject.getName().getLocalPart();
						
						//System.out.println("XmlObject NameLocal: " + key);
						sobjectFieldMap.put(key.toUpperCase(), key);
					}
					
					if (sobjectFieldMap.containsKey(pathItem.toUpperCase())) {
						pathItemCaseSensitive = sobjectFieldMap.get(pathItem.toUpperCase());
					} else {
						pathItemCaseSensitive = pathItem;
					}
					
					//System.out.println("pathItem: " + pathItem);
					//System.out.println("pathItemCaseSensitive: " + pathItemCaseSensitive);
					
					currentRecord = (SObject) currentRecord.getSObjectField(pathItemCaseSensitive);
				}
			} else {
				if (currentRecord != null) {
					Map<String,String> sobjectFieldMap = new HashMap<>();
					Iterator<XmlObject> iter;
					String pathItemCaseSensitive;
					
					iter = currentRecord.getChildren();
					
					while (iter.hasNext()) {
						XmlObject xmlObject = iter.next();
						String key = xmlObject.getName().getLocalPart();
						
						sobjectFieldMap.put(key.toUpperCase(), key);
					}
					
					if (sobjectFieldMap.containsKey(pathItem.toUpperCase())) {
						pathItemCaseSensitive = sobjectFieldMap.get(pathItem.toUpperCase());
					} else {
						pathItemCaseSensitive = pathItem;
					}
					
					//System.out.println("currentRecord: " + currentRecord + ", pathItem: " + pathItem);
					//System.out.println("currentRecord: " + currentRecord + ", pathItemCaseSensitive: " + pathItemCaseSensitive);
					
					XmlObject fieldXmlObject = currentRecord.getChild(pathItemCaseSensitive);
					
					if (fieldXmlObject != null) {
						//System.out.println("fieldXmlObject: " + fieldXmlObject);
						
						result = currentRecord.getSObjectField(pathItemCaseSensitive);
						
						if (result == null) {
							Object result2 = fieldXmlObject.getField(pathItemCaseSensitive);
							
							if (result2 != null) {
								result = result2;
							}
						}
						
						//System.out.println("result: " + result);
					} else {
						throw new RuntimeException("Field " + pathItemCaseSensitive + " not found at " + currentPathItem);
					}
				}
				
				return result;
			}
			
			pathIndex++;
		}
		
		return result;
	}
	
	public ObjectDescribe describeObject(String objectName) throws ConnectionException {
		ObjectDescribe result = new ObjectDescribe();
		DescribeSObjectResult describe = partnerConnection.describeSObject(objectName);
		Field[] fields = describe.getFields();
		
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			FieldType fieldType = field.getType();
			FieldDescribe resultItem = new FieldDescribe();
			
			resultItem.name = field.getName();
			resultItem.label = field.getLabel();
			resultItem.type = fieldType.name();
			
			resultItem.extraTypeInfo = field.getExtraTypeInfo();
			resultItem.soapType = field.getSoapType().name();
			resultItem.isAutoNumber = field.getAutoNumber();
			resultItem.isNillable = field.isNillable();
			resultItem.isCalculated = field.isCalculated();
			resultItem.isCaseSensitive = field.isCaseSensitive();
			resultItem.isCreate = field.isCreateable();
			resultItem.isCustom = field.isCustom();
			resultItem.isEncrypted = field.isEncrypted();
			resultItem.isExternalId = field.isExternalId();
			resultItem.isDeprecatedAndHidden = field.isDeprecatedAndHidden();
			resultItem.byteLength = field.getByteLength();
			resultItem.digits = field.getDigits();
			resultItem.length = field.getLength();
			resultItem.precision = field.getPrecision();

			if (field.getReferenceTo() != null) {
				String[] refTo = field.getReferenceTo();
				
				if (refTo != null && refTo.length > 0) {
					for (String str : refTo) {
						resultItem.referenceTo.add(str);
					}
				}
			}
			
			result.fieldList.add(resultItem);
		}
		
		return result;
	}
	
	public List<FileProperties> listMetadata(String type, String folder) throws Exception {
		MetadataConnection metadataConnection = getMetadataConnection();
		ListMetadataQuery listMetadataQuery = new ListMetadataQuery();
		Double asOfVersion = Double.valueOf(SFDC_API_VERSION);
		FileProperties[] result;
		List<FileProperties> resultList = new ArrayList<>();
		
		listMetadataQuery.setType(type);
		listMetadataQuery.setFolder(folder);
		
		result = metadataConnection.listMetadata(new ListMetadataQuery[] { listMetadataQuery } , asOfVersion);
		
		if (result != null) {
			for (FileProperties fileProperties : result) {
				resultList.add(fileProperties);
			}
		}
		
		return resultList;
	}
	
	public List<FileProperties> listBigObjects() throws Exception {
		List<FileProperties> resultList = new ArrayList<>();
		ListMetadataQuery listMetadataQuery = new ListMetadataQuery();
		Double asOfVersion = Double.valueOf(SFDC_API_VERSION);
		FileProperties[] result;
		
		listMetadataQuery.setType("CustomObject");
		listMetadataQuery.setFolder(null);
		
		result = metadataConnection.listMetadata(new ListMetadataQuery[] { listMetadataQuery } , asOfVersion);
		
		if (result != null) {
			for (FileProperties fileProperties : result) {
				if (fileProperties.getFullName().endsWith("__b")) {
					resultList.add(fileProperties);
				}
			}
		}
				
		return resultList;
	}
	
	public XmlFile getBigObjectPackageXml(BigObject bo) {	
		XmlFile resultFile = new XmlFile();
		List<String> contentList = new ArrayList<>();
		
		contentList.add("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		contentList.add("<Package xmlns=\"http://soap.sforce.com/2006/04/metadata\">");
		contentList.add("\t<types>");
		contentList.add("\t\t<members>*</members>");
		contentList.add("\t\t<name>CustomObject</name>");
		contentList.add("\t</types>");
		contentList.add("\t<types>");
		contentList.add("\t\t<members>*</members>");
		contentList.add("\t\t<name>PermissionSet</name>");
		contentList.add("\t</types>");
		contentList.add("\t<version>41.0</version>");
		contentList.add("</Package>");
	
		resultFile.directory = "src";
		resultFile.filename = "package.xml";
		resultFile.content = String.join("\n", contentList); 
		
		return resultFile;
	}
	
	public XmlFile getBigObjectPermissionSetXml(BigObject bo) {	
		XmlFile resultFile = new XmlFile();
		List<String> contentList = new ArrayList<>();
		
		contentList.add("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		contentList.add("<PermissionSet xmlns=\"http://soap.sforce.com/2006/04/metadata\">");
		
		contentList.add("\t<label>" + bo.label + " Permission Set</label>");
		
		for (BigObjectCustomField customField : bo.fields) {
			contentList.add("\t<fieldPermissions>");
			contentList.add("\t\t<field>" + bo.fullName + "." + customField.fullName + "</field>");
			contentList.add("\t\t<editable>true</editable>");
			contentList.add("\t\t<readable>true</readable>");
			contentList.add("\t</fieldPermissions>");
		}
		
		contentList.add("</PermissionSet>");
	
		resultFile.directory = "src/permissionsets";
		resultFile.filename = bo.fullName + "_BigObject.permissionset";
		resultFile.content = String.join("\n", contentList); 
		
		return resultFile;
	}
	
	public XmlFile getBigObjectXml(BigObject bo) {
		XmlFile resultFile = new XmlFile();
		List<String> contentList = new ArrayList<>();
		
		contentList.add("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		contentList.add("<CustomObject xmlns=\"http://soap.sforce.com/2006/04/metadata\">");
		contentList.add("\t<deploymentStatus>Deployed</deploymentStatus>");
		
		contentList.add("\t<label>" + bo.label + "</label>");
		contentList.add("\t<pluralLabel>" + bo.pluralLabel + "</pluralLabel>");
		
		for (BigObjectCustomField customField : bo.fields) {
			Boolean isNumeric = false;
			Boolean isLength = false;
			Boolean isLookup = false;
			String type = customField.type.name();
			
			if (type.equalsIgnoreCase("Number")) {
				isNumeric = true;
			}
			
			if (type.equalsIgnoreCase("Text") || type.equalsIgnoreCase("LongTextArea")) {
				isLength = true;
			}
			
			if (type.equalsIgnoreCase("Lookup")) {
				isLookup = true;
			}
			
			contentList.add("\t<fields>");
			contentList.add("\t\t<fullName>" + customField.fullName + "</fullName>");
			contentList.add("\t\t<label>" + customField.label + "</label>");
			//contentList.add("\t\t<pluralLabel>" + customField.pluralLabel + "</pluralLabel>");
			contentList.add("\t\t<required>" + customField.required + "</required>");
			contentList.add("\t\t<type>" + customField.type.name() + "</type>");
			contentList.add("\t\t<externalId>" + customField.externalId + "</externalId>");
			contentList.add("\t\t<unique>" + customField.unique + "</unique>");
			
			if (isNumeric) {
				contentList.add("\t\t<scale>" + + customField.scale + "</scale>");
				contentList.add("\t\t<precision>" + customField.precision + "</precision>");
			}
			
			if (isLookup) {
				contentList.add("\t\t<referenceTo>" + customField.referenceTo + "</referenceTo>");
				contentList.add("\t\t<relationshipName>" + customField.relationshipName + "</relationshipName>");
			}
			
			if (isLength) {
				contentList.add("\t\t<length>" + customField.length + "</length>");
			}
			
			contentList.add("\t</fields>");
		}
		
		contentList.add("\t<indexes>");
		contentList.add("\t\t<fullName>" + bo.fullName.replace("__b", "") + "Index" + "</fullName>");
		contentList.add("\t\t<label>" + bo.fullName.replace("__b",  "")  + " Index" + "</label>");
		
		for (Index index : bo.indexes) {
			contentList.add("\t\t<fields>");
			contentList.add("\t\t\t<name>" + index.name + "</name>");
			contentList.add("\t\t\t<sortDirection>" + index.sortDirection.name() + "</sortDirection>");
			contentList.add("\t\t</fields>");
		}
		
		contentList.add("\t</indexes>");
		
		
		contentList.add("</CustomObject>");
		
		resultFile.directory = "src/objects";
		resultFile.filename = bo.fullName + ".object";
		resultFile.content = String.join("\n", contentList); 
		
		return resultFile;
	}
	
	public DeployResult waitForDeployCompletion(String asyncResultId) throws Exception {
		int poll = 0;
		long waitTimeMillisecs = 1000;
		DeployResult deployResult;
		boolean fetchDetails;
		
		do {
			Thread.sleep(waitTimeMillisecs);
			
			if (poll++ > 120) {
				throw new RuntimeException("Request timed out. If this is a large set of metadata components, ensure MAX_NUM_POLL_REQUESTS is big enough.");
			}
			
			fetchDetails = (poll % 3 == 0);
			
			deployResult = metadataConnection.checkDeployStatus(asyncResultId, fetchDetails);
			
			//System.out.println("Status is: " + deployResult.getStatus() + " [pollNum=" + poll + "]");
			
			if (deployResult.isDone() == false && fetchDetails) {
				printErrors(deployResult, "Failures for deployment in progress:\n");
			}
		} while (deployResult.isDone() == false);
		
		if (deployResult.isSuccess() == false && deployResult.getErrorStatusCode() != null) {
			throw new RuntimeException(deployResult.getErrorStatusCode() + " msg: " + deployResult.getErrorMessage());
		}
		
		if (fetchDetails == false) {
			deployResult = metadataConnection.checkDeployStatus(asyncResultId, true);
		}
		
		return deployResult;
	}
	
	public void printErrors(DeployResult result, String messageHeader) {
		DeployDetails details = result.getDetails();
		StringBuilder stringBuilder = new StringBuilder();
		
		if (details != null) {
			DeployMessage[] componentFailures = details.getComponentFailures();
			for (DeployMessage failure : componentFailures) {
				String loc = "(" + failure.getLineNumber() + ", " + failure.getColumnNumber();
				
				if (loc.length() == 0 && ! failure.getFileName().equals(failure.getFullName())) {
					loc = "(" + failure.getFullName() + ")";
				}
				
				stringBuilder.append(failure.getFileName() + loc + ":" + failure.getProblem()).append("\n");
			}
			RunTestsResult rtr = details.getRunTestResult();
			
			if (rtr.getFailures() != null) {
				for (RunTestFailure failure : rtr.getFailures()) {
					String n = (failure.getNamespace() == null ? "" : (failure.getNamespace() + ".")) + failure.getName();
					stringBuilder.append("Test failure, method: " + n + "." + failure.getMethodName() + " -- " + failure.getMessage() + " stack " + failure.getStackTrace() + "\n\n");
				}
			}
			
			if (rtr.getCodeCoverageWarnings() != null) {
				for (CodeCoverageWarning ccw : rtr.getCodeCoverageWarnings()) {
					stringBuilder.append("Code coverage issue");
					
					if (ccw.getName() != null) {
						String n = (ccw.getNamespace() == null ? "" : (ccw.getNamespace() + ".")) + ccw.getName();
						stringBuilder.append(", class:" + n);
					}
					stringBuilder.append(" -- " + ccw.getMessage() + "\n");
				}
			}
		}
		
		if (stringBuilder.length() > 0) {
			stringBuilder.insert(0,  messageHeader);
			
			System.out.println(stringBuilder.toString());
		}
	}
	
	public String getErrors(DeployResult result, String messageHeader) {
		DeployDetails details = result.getDetails();
		StringBuilder stringBuilder = new StringBuilder();
		
		if (details != null) {
			DeployMessage[] componentFailures = details.getComponentFailures();
			for (DeployMessage failure : componentFailures) {
				String loc = "(" + failure.getLineNumber() + ", " + failure.getColumnNumber();
				
				if (loc.length() == 0 && ! failure.getFileName().equals(failure.getFullName())) {
					loc = "(" + failure.getFullName() + ")";
				}
				
				stringBuilder.append(failure.getFileName() + loc + ":" + failure.getProblem()).append("\n");
			}
			RunTestsResult rtr = details.getRunTestResult();
			
			if (rtr.getFailures() != null) {
				for (RunTestFailure failure : rtr.getFailures()) {
					String n = (failure.getNamespace() == null ? "" : (failure.getNamespace() + ".")) + failure.getName();
					stringBuilder.append("Test failure, method: " + n + "." + failure.getMethodName() + " -- " + failure.getMessage() + " stack " + failure.getStackTrace() + "\n\n");
				}
			}
			
			if (rtr.getCodeCoverageWarnings() != null) {
				for (CodeCoverageWarning ccw : rtr.getCodeCoverageWarnings()) {
					stringBuilder.append("Code coverage issue");
					
					if (ccw.getName() != null) {
						String n = (ccw.getNamespace() == null ? "" : (ccw.getNamespace() + ".")) + ccw.getName();
						stringBuilder.append(", class:" + n);
					}
					stringBuilder.append(" -- " + ccw.getMessage() + "\n");
				}
			}
		}
		
		if (stringBuilder.length() > 0) {
			stringBuilder.insert(0,  messageHeader);
			
			return stringBuilder.toString();
		}
		
		return null;
	}

	
	public boolean deployZip(String filename) throws Exception {
		DeployOptions deployOptions = new DeployOptions();
		AsyncResult asyncResult;
		DeployResult deployResult;
		byte zipBytes[];
		MetadataConnection metadataConnection = getMetadataConnection();
		boolean result = false;
		String errorMessage;
		
		zipBytes = Util.readZipFile(filename);
		
		deployOptions.setPerformRetrieve(false);
		deployOptions.setRollbackOnError(true);
		
		asyncResult = metadataConnection.deploy(zipBytes, deployOptions);
		
		deployResult = waitForDeployCompletion(asyncResult.getId());
		
		if (deployResult.isSuccess() == false) {
			errorMessage = getErrors(deployResult, "Final list of failures:\n");
			
			throw new RuntimeException("The deployment failed with the following error(s)\n\n" + errorMessage);
			
			//throw new RuntimeException("The files were not successfully deployed");
		} else {
			System.out.println("The file " + filename + " was successfully deployed\n");
			
			result = true;
		}
		
		return result;

	}
	
	public void retrieveZip(String packageFile, String targetZipFilename) throws Exception {
		RetrieveRequest rr = new RetrieveRequest();
		AsyncResult ar;
		int poll;
		long wtMs;
		RetrieveResult res = null;
		SoapFaultException soapFault;
		
		rr.setApiVersion(Float.valueOf(SFDC_API_VERSION));
		
		setUnpackaged(packageFile, rr);
		
		ar = metadataConnection.retrieve(rr);
		
		// wait for the retrieve to complete
		poll = 0;
		wtMs = ONE_SECOND;
		
		do {
			Thread.sleep(wtMs);
			// double the wait time for the next iteration
			wtMs *= 2;
			
			if (poll++ > MAX_NUM_POLL_REQUESTS) {
				throw new RuntimeException("Request timed out.");
			}
			
			soapFault = null;
			try {
				res = null;
				res = metadataConnection.checkRetrieveStatus(ar.getId(), true);
			} catch (SoapFaultException e) {
				soapFault = e;
			}
			
			if (res != null) {
				System.out.println("Retrieve Status: " + res.getStatus());
			}
			
			if (soapFault != null) {
				System.out.println("Retrieve Soap Fault: " + soapFault.getMessage());
			}
		} while (soapFault != null || res != null && res.isDone() == false);
		
		if (res == null) {
			res = metadataConnection.checkRetrieveStatus(ar.getId(), true);
		}
		
		// Print any warning messages
		{
			StringBuilder sb = new StringBuilder();
			
			if (res.getMessages() != null) {
				for (RetrieveMessage rm : res.getMessages()) {
					sb.append(rm.getFileName() + ": " + rm.getProblem());
				}
			}
			
			if (sb.length() > 0) {
				System.out.println("Retrieve Warnings:\n" + sb);
			}
			
			// Write the zip to filesystem
			System.out.println("Writing results to zip file");
			
			{
				ByteArrayInputStream bais = new ByteArrayInputStream(res.getZipFile());
				File f = new File(targetZipFilename);
				FileOutputStream fos = new FileOutputStream(f);
				
				try {
					ReadableByteChannel src = Channels.newChannel(bais);
					FileChannel dst = fos.getChannel();
					
					copy(src, dst);
					
					System.out.println("Results written to " + f.getAbsolutePath());
				} finally {
					fos.close();
				}
			}
		}
	}
	
	void setUnpackaged(String packageFile, RetrieveRequest request) throws Exception {
		// Edit the path, if necessary, if your package.xml file is located
		// elsewhere
		//File unpackedManifest = new File(Util.getAbsolutePath(MANIFEST_FILE));
		InputStream is;
		
		if (packageFile != null) {
			is = new FileInputStream(packageFile);
		} else {
			is = Util.getResourceInputStream(MANIFEST_FILE);
		}
		
		/*
		System.out.println("Manifest file: " + unpackedManifest.getAbsolutePath());
		if (!unpackedManifest.exists() || !unpackedManifest.isFile())
			throw new Exception("Should provide a valid retrieve manifest " + "for unpackaged content. "
					+ "Looking for " + unpackedManifest.getAbsolutePath());
*/
		
		// Note that we populate the _package object by parsing a manifest file
		// here.
		// You could populate the _package based on any source for your
		// particular application.
		com.sforce.soap.metadata.Package p = parsePackage(is);
		
		is.close();
		
		request.setUnpackaged(p);
	}
	
	com.sforce.soap.metadata.Package parsePackage(InputStream is) throws Exception {
		try {
			List<PackageTypeMembers> pd = new ArrayList<PackageTypeMembers>();
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Element d = db.parse(is).getDocumentElement();
			for (Node c = d.getFirstChild(); c != null; c = c.getNextSibling()) {
				if (c instanceof Element) {
					Element ce = (Element) c;
					//
					NodeList namee = ce.getElementsByTagName("name");
					if (namee.getLength() == 0) {
						// not
						continue;
					}
					String name = namee.item(0).getTextContent();
					NodeList m = ce.getElementsByTagName("members");
					List<String> members = new ArrayList<String>();
					for (int i = 0; i < m.getLength(); i++) {
						Node mm = m.item(i);
						members.add(mm.getTextContent());
					}
					PackageTypeMembers pdi = new PackageTypeMembers();
					pdi.setName(name);
					pdi.setMembers(members.toArray(new String[members.size()]));
					pd.add(pdi);
				}
			}
			com.sforce.soap.metadata.Package r = new com.sforce.soap.metadata.Package();
			r.setTypes(pd.toArray(new PackageTypeMembers[pd.size()]));
			r.setVersion(METADATA_API_VERSION + "");
			return r;
		} catch (ParserConfigurationException pce) {
			throw new Exception("Cannot create XML parser", pce);
		} catch (IOException ioe) {
			throw new Exception(ioe);
		} catch (SAXException se) {
			throw new Exception(se);
		}
	}

	
	void copy(ReadableByteChannel src, WritableByteChannel dest) throws IOException {
		// use an in-memory byte buffer
		ByteBuffer buffer = ByteBuffer.allocate(8092);
		while (src.read(buffer) != -1) {
			buffer.flip();
			while (buffer.hasRemaining()) {
				dest.write(buffer);
			}
			buffer.clear();
		}
	}

	public String retrieveXml(String type, String fullName) throws Exception {
		String resultXml = null;
		String uniqueId = Util.getUniqueId();
		String temporaryDirectory = File.separator + "tmp" + File.separator + uniqueId;
		String temporaryZipFile = File.separator + "tmp" + File.separator + uniqueId + ".zip";
		String temporaryUnzipDirectory = File.separator + "tmp" + File.separator + uniqueId + ".tmp";
		String srcDirectory = temporaryDirectory + File.separator + "src";
		String packageFilename = temporaryDirectory + File.separator + uniqueId + "_package.xml";
		String packageXmlContent = "";
		List<String> files;
		String resultXmlFilename;
		
		if (Util.directoryExists(temporaryDirectory) == false) {
			Util.mkdir(temporaryDirectory);
		}
		
		packageXmlContent += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		packageXmlContent += "<Package xmlns=\"http://soap.sforce.com/2006/04/metadata\">\n";
		packageXmlContent += "\t<types>\n";
		packageXmlContent += "\t\t<members>" + fullName + "</members>\n";
		packageXmlContent += "\t\t<name>" + type + "</name>\n";
		packageXmlContent += "\t</types>\n";
		packageXmlContent += "</Package>\n";
		
		FileOutputStream fos = new FileOutputStream(packageFilename);
		fos.write(packageXmlContent.getBytes());
		fos.close();
		
		retrieveZip(packageFilename, temporaryZipFile);
		
		Util.unzip(temporaryZipFile, temporaryUnzipDirectory);
		
		if (Util.directoryExists(srcDirectory) == false) {
			Util.mkdir(srcDirectory);
		}
		
		files = Util.getDirectoryFiles(temporaryUnzipDirectory);
		
		if (files != null && files.isEmpty() == false) {
			for (String file : files) {
				String filename = temporaryUnzipDirectory + File.separator + file;
				String targetFilename = srcDirectory  + File.separator + file;
				String dirname = Util.dirname(targetFilename);
				
				if (Util.directoryExists(dirname) == false) {
					Util.mkdir(dirname);
				}
				
				Util.mvfile(filename, targetFilename);
			}
		}
		
		Util.rmfile(temporaryZipFile);
		Util.rmfile(packageFilename);
		
		resultXmlFilename = srcDirectory + File.separator + "unpackaged" + File.separator + "objects" + File.separator + fullName + ".object";
		
		resultXml = Util.getFileContent(resultXmlFilename);
		
		Util.rmdir(temporaryDirectory, true);
				
		return resultXml;
	}
	
	public BigObject fetchBigObject(String boFullName) throws Exception {
		BigObject bo = new BigObject();
		String boXml = retrieveXml("CustomObject", boFullName);
		
		System.out.println(boXml);
		
		bo = BigObject.parseXml(boFullName, boXml);
		
		return bo;
	}
}
