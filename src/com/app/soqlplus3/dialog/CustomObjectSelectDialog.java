package com.app.soqlplus3.dialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.sforce.soap.metadata.AsyncResult;
import com.sforce.soap.metadata.DeployOptions;
import com.sforce.soap.metadata.DeployResult;
import com.sforce.soap.metadata.FileProperties;
import com.sforce.soap.metadata.MetadataConnection;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import com.app.soqlplus3.Util;
import com.app.soqlplus3.model.BigObjectCustomField;

public class CustomObjectSelectDialog {
	final static int WIDTH = 400;
	final static int HEIGHT = 300;
	final static String TITLE = "Edit Object";
	final static Insets INSETS = new Insets(8);

	// JavaFX Controls
	Stage parentStage;
	Stage stage;
	Scene scene;

	Button selectButton;
	Button cancelButton;

	GridPane grid;

	TextField name = new TextField();

	TableView<FileProperties> table;
	
	public String selectedEntry;

	public CustomObjectSelectDialog() {
		initControls();
		initButtonHandlers();
		
		selectedEntry = null;
	}

	void initControls() {
		selectButton = new Button("Select");
		cancelButton = new Button("Cancel");
	}

	void initButtonHandlers() {
		selectButton.setOnAction((event) -> {
			eventSelect();
		});
		cancelButton.setOnAction((event) -> {
			eventCancel();
		});
	}

	void eventSelect() {
		FileProperties item = table.getSelectionModel().getSelectedItem();
		
		table.refresh();
		
		if (item != null) {
			if (Util.isEmpty(item.getNamespacePrefix())) {
				selectedEntry = item.getFullName();
			} else {
				if (item.getFullName().startsWith(item.getNamespacePrefix())) {
					selectedEntry = item.getFullName();
				} else {
					selectedEntry = item.getNamespacePrefix() + "__" + item.getFullName();
				}
			}
		}

		stage.close();
	}

	void eventCancel() {
		stage.close();
	}

	public static CustomObjectSelectDialog show(Stage parentStage, List<FileProperties> objects) {
		TableView<FileProperties> table = new TableView<FileProperties>();

		CustomObjectSelectDialog dialog = new CustomObjectSelectDialog();

		table.setItems(FXCollections.observableArrayList(objects));
		
		dialog.table = table;

		dialog.setStage(parentStage);

		return dialog;
	}

	void setStage(Stage parentStage) {
		this.parentStage = parentStage;

		stage = new Stage();

		stage.setTitle(TITLE);
		stage.initModality(Modality.WINDOW_MODAL);
		stage.initOwner(parentStage);

		table.setPrefWidth(WIDTH - 16);

		grid = new GridPane();
		grid.setPadding(INSETS);

		initTable(table);
		initContent(grid);

		scene = new Scene(grid, WIDTH, HEIGHT);

		stage.setScene(scene);
		stage.showAndWait();
	}

	@SuppressWarnings("unchecked")
	void initTable(TableView<FileProperties> table) {
		TableColumn<FileProperties, String> tableColumn1 = new TableColumn<FileProperties, String>("Namespace");
		TableColumn<FileProperties, String> tableColumn2 = new TableColumn<FileProperties, String>("API Name");
		

		table.getColumns().addAll(tableColumn1, tableColumn2);
	

		tableColumn1.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<FileProperties, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<FileProperties, String> param) {
						if (param == null || param.getValue() == null) {
							return null;
						}

						return new SimpleStringProperty(
								param.getValue().getNamespacePrefix()
						);
					}
				});
		
		tableColumn2.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<FileProperties, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<FileProperties, String> param) {
						if (param == null || param.getValue() == null) {
							return null;
						}

						return new SimpleStringProperty(
								Util.isEmpty(param.getValue().getNamespacePrefix())
									? param.getValue().getFullName()
									: (
											param.getValue().getFullName().startsWith(param.getValue().getNamespacePrefix())
											? param.getValue().getFullName().substring(param.getValue().getNamespacePrefix().length() + "__".length(), param.getValue().getFullName().length())
											: param.getValue().getFullName()
									)
						);
					}
				});
		
	}

	void initContent(GridPane grid) {
		HBox buttons = new HBox();

		buttons.setAlignment(Pos.CENTER);

		buttons.getChildren().addAll(selectButton, cancelButton);

		grid.add(new Label("Please select the Object to edit."), 0, 0);
		grid.add(table, 0, 1, 2, 1);
		grid.add(buttons, 0, 2, 2, 1);
	}
}
