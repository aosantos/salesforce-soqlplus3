package com.app.soqlplus3.dialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.sforce.soap.metadata.AsyncResult;
import com.sforce.soap.metadata.DeployOptions;
import com.sforce.soap.metadata.DeployResult;
import com.sforce.soap.metadata.MetadataConnection;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import com.app.soqlplus3.Salesforce;
import com.app.soqlplus3.Util;
import com.app.soqlplus3.XmlFile;
import com.app.soqlplus3.model.BigObject;
import com.app.soqlplus3.model.BigObjectCustomField;
import com.app.soqlplus3.model.Index;

public class BigObjectDialog {
	final static int WIDTH = 800;
	final static int HEIGHT = 600;
	final static String TITLE = "Edit Big Object";
	final static Insets INSETS = new Insets(8);
	
	TableView<BigObjectCustomField> fields = new TableView<>();
	TableView<Index> indexes = new TableView<>();
	
	// JavaFX Controls
	Stage parentStage;
	Stage stage;
	Scene scene;
	
	Button saveButton;
	Button cancelButton;
	Button addFieldButton;
	Button editFieldButton;
	Button deleteFieldButton;
	Button addIndexButton;
	Button editIndexButton;
	Button deleteIndexButton;
	
	GridPane grid;
	
	Label label = new Label("Label");
	Label pluralLabel = new Label("Plural Label");
	Label fullNameLabel = new Label("Full Name");
	Label fieldsLabel = new Label("Fields");
	Label indexesLabel = new Label("Indexes");
	
	TextField bigObjectLabel = new TextField();
	TextField bigObjectPluralLabel = new TextField();
	TextField bigObjectFullName = new TextField();
	
	Salesforce salesforce;
	
	BigObjectDialog() {
		initControls();
		initButtonHandlers();
	}
	
	void initControls() {
		saveButton = new Button("Save");
		cancelButton = new Button("Cancel");
		
		addFieldButton = new Button("Add Field");
		editFieldButton = new Button("Edit Field");
		deleteFieldButton = new Button("Delete Field");

		addIndexButton = new Button("Add Index");
		editIndexButton = new Button("Edit Index");
		deleteIndexButton = new Button("Delete Index");
		
		fields.setPrefWidth(WIDTH - 16);
		indexes.setPrefWidth(WIDTH - 16);
	}
	
	void initButtonHandlers() {
		saveButton.setOnAction((event) -> { try {
			eventSave();
		} catch (Throwable t) {
			System.out.println("Deploy Failed, check the errors from the deployment:\n" + t.getMessage());
			
			ExceptionDialog.showDialog("Deploy Failed", "Check the errors from the deployment",
					t.getMessage(), t, "ok");
		} });
		cancelButton.setOnAction((event) -> { eventCancel(); });
		
		addFieldButton.setOnAction((event) -> { eventAddField(); });
		editFieldButton.setOnAction((event) -> { eventEditField(); });
		deleteFieldButton.setOnAction((event) -> { eventDeleteField(); });
		
		addIndexButton.setOnAction((event) -> { eventAddIndex(); });
		editIndexButton.setOnAction((event) -> { eventEditIndex(); });
		deleteIndexButton.setOnAction((event) -> { eventDeleteIndex(); });
	}
	
	public static BigObjectDialog show(Stage parentStage, Salesforce salesforce) {
		BigObjectDialog dialog = new BigObjectDialog();
		
		dialog.salesforce = salesforce;
		dialog.setStage(parentStage);
		
		return dialog;
	}
	
	public static BigObjectDialog show(Stage parentStage, Salesforce salesforce, BigObject bo) {
		BigObjectDialog dialog = new BigObjectDialog();
		
		dialog.bigObjectFullName.setText(bo.fullName);
		dialog.bigObjectLabel.setText(bo.label);
		dialog.bigObjectPluralLabel.setText(bo.pluralLabel);
		dialog.fields.setItems(FXCollections.observableArrayList(bo.fields));
		dialog.indexes.setItems(FXCollections.observableArrayList(bo.indexes));
		
		dialog.salesforce = salesforce;
		dialog.setStage(parentStage);
		
		return dialog;
	}
	
	void setStage(Stage parentStage) {
		this.parentStage = parentStage;
		
		stage = new Stage();
		
		stage.setTitle(TITLE);
		stage.initModality(Modality.WINDOW_MODAL);
		stage.initOwner(parentStage);
		
		grid = new GridPane();
		grid.setPadding(INSETS);
		
		initContent(grid);
		
		scene = new Scene(grid, WIDTH, HEIGHT);
		
		stage.setScene(scene);
		stage.showAndWait();
	}
	
	void initContent(GridPane grid) {
		HBox buttons = new HBox();
		HBox fieldsButtons = new HBox();
		HBox indexesButtons = new HBox();
		
		buttons.setAlignment(Pos.CENTER);
		fieldsButtons.setAlignment(Pos.CENTER);
		indexesButtons.setAlignment(Pos.CENTER);
		
		buttons.getChildren().addAll(saveButton, cancelButton);
		
		fieldsButtons.getChildren().addAll(addFieldButton, editFieldButton, deleteFieldButton);
		
		indexesButtons.getChildren().addAll(addIndexButton, editIndexButton, deleteIndexButton);
		
		
		grid.add(label, 0, 0);
		grid.add(pluralLabel, 0, 1);
		grid.add(fullNameLabel, 0, 2);
		grid.add(buttons, 0, 3, 2, 1);
		grid.add(fieldsLabel, 0, 4);
		grid.add(indexesLabel, 0, 7);
		
		grid.add(bigObjectLabel, 1, 0);
		grid.add(bigObjectPluralLabel, 1, 1);
		grid.add(bigObjectFullName, 1, 2);
		grid.add(fieldsButtons, 0, 5, 2, 1);
		grid.add(fields, 0, 6, 2, 1);
		grid.add(indexesButtons, 0, 8, 2, 1);
		grid.add(indexes, 0, 9, 2, 1);
		
		initFields();
		initIndexes();
	}
	
	void initFields() {
		String[] fieldColumns = new String[] {
			"Label", /*"Plural Label",*/ "Full Name", "Required?", "Type", "Scale", "Precision", "Length", "Reference To", "Relationship Name"
		};
		
		//fields.getSelectionModel().setCellSelectionEnabled(true);
		
		for (String fieldColumn : fieldColumns) {
			TableColumn<BigObjectCustomField, String> tableColumn = new TableColumn<>(fieldColumn);
			
			fields.getColumns().add(tableColumn);
			
			tableColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<BigObjectCustomField,String>, ObservableValue<String>>() {
				
				@Override
				public ObservableValue<String> call(CellDataFeatures<BigObjectCustomField, String> param) {
					if (param == null || param.getValue() == null) {
						return null;
					}
					
					if (param != null && param.getValue() != null && param.getTableColumn().getText().equalsIgnoreCase("Label")) {
						return new SimpleStringProperty(param.getValue().label);
					}
					
					/*
					if (param != null && param.getValue() != null && param.getTableColumn().getText().equalsIgnoreCase("Plural Label")) {
						return new SimpleStringProperty(param.getValue().pluralLabel);
					}*/
					
					if (param != null && param.getValue() != null && param.getTableColumn().getText().equalsIgnoreCase("Full Name")) {
						return new SimpleStringProperty(param.getValue().fullName);
					}

					if (param != null && param.getValue() != null && param.getTableColumn().getText().equalsIgnoreCase("Required?")) {
						return new SimpleStringProperty(param.getValue().required ? "true" : "false");
					}
					
					if (param != null && param.getValue() != null && param.getTableColumn().getText().equalsIgnoreCase("Type")) {
						return new SimpleStringProperty(String.valueOf(param.getValue().type.name()));
					}
					
					if (param != null && param.getValue() != null && param.getTableColumn().getText().equalsIgnoreCase("Scale")) {
						return new SimpleStringProperty(String.valueOf(param.getValue().scale));
					}
					
					if (param != null && param.getValue() != null && param.getTableColumn().getText().equalsIgnoreCase("Precision")) {
						return new SimpleStringProperty(String.valueOf(param.getValue().precision));
					}
					
					if (param != null && param.getValue() != null && param.getTableColumn().getText().equalsIgnoreCase("Length")) {
						return new SimpleStringProperty(String.valueOf(param.getValue().length));
					}
					
					if (param != null && param.getValue() != null && param.getTableColumn().getText().equalsIgnoreCase("Reference To")) {
						return new SimpleStringProperty(param.getValue().referenceTo);
					}
					
					if (param != null && param.getValue() != null && param.getTableColumn().getText().equalsIgnoreCase("Relationship Name")) {
						return new SimpleStringProperty(param.getValue().relationshipName);
					}
					
					return null;
				}
			});
		}
	}
	
	void initIndexes() {
		String[] fieldColumns = new String[] {
				"Name", "Sort Direction"
			};
			
			for (String fieldColumn : fieldColumns) {
				TableColumn<Index, String> tableColumn = new TableColumn<>(fieldColumn);
				
				indexes.getColumns().add(tableColumn);
				
				tableColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Index,String>, ObservableValue<String>>() {
					
					@Override
					public ObservableValue<String> call(CellDataFeatures<Index, String> param) {
						if (param == null || param.getValue() == null) {
							return null;
						}
						
						if (param != null && param.getValue() != null && param.getTableColumn().getText().equalsIgnoreCase("Name")) {
							return new SimpleStringProperty(param.getValue().name);
						}
						
						if (param != null && param.getValue() != null && param.getTableColumn().getText().equalsIgnoreCase("Sort Direction")) {
							return new SimpleStringProperty(param.getValue().sortDirection.name());
						}
						
						return null;
					}
				});
			}

	}
	
	void eventSave() throws Exception {
		BigObject bo = new BigObject();
		XmlFile packageXmlFile;
		//XmlFile permissionSetXmlFile;
		XmlFile customObjectXmlFile;
		List<XmlFile> xmlFileList = new ArrayList<>();
		String zipFilename;
		byte[] zipFile;
		DeployOptions deployOptions = new DeployOptions();
		AsyncResult asyncResult;
		DeployResult deployResult;
		boolean deploySuccess;
		deployOptions.setPerformRetrieve(false);
		deployOptions.setRollbackOnError(true);
		
		// copy from UI to model
		bo.fullName = bigObjectFullName.getText();
		bo.label = bigObjectLabel.getText();
		bo.pluralLabel = bigObjectPluralLabel.getText();
		bo.fields = fields.getItems();
		bo.indexes = indexes.getItems();
		
		packageXmlFile = salesforce.getBigObjectPackageXml(bo);
		//permissionSetXmlFile = salesforce.getBigObjectPermissionSetXml(bo);
		customObjectXmlFile = salesforce.getBigObjectXml(bo);
		
		System.out.println("Content for file " + customObjectXmlFile.filename + "\n" + customObjectXmlFile.content);
		
		xmlFileList.add(packageXmlFile);
		//xmlFileList.add(permissionSetXmlFile);
		xmlFileList.add(customObjectXmlFile);
		
		zipFilename = Util.getZipFile(xmlFileList);
		
		deploySuccess = salesforce.deployZip(zipFilename);
		
		Util.rmfile(zipFilename);
		
		System.out.println("Deploy " + (deploySuccess ? " with Success" : " has Failed!"));
		
		stage.close();
	}
	
	void eventCancel() {
		stage.close();
	}
	
	void eventAddField() {
		ObservableList<BigObjectCustomField> items = fields.getItems();
		
		if (items == null) {
			items = FXCollections.observableList(new ArrayList<BigObjectCustomField>());
		}
		
		items.add(new BigObjectCustomField());
		
		fields.setItems(items);
	}
	
	void eventEditField() {
		BigObjectCustomField item;
		
		item = fields.getSelectionModel().getSelectedItem();
		
		if (item != null) {
			System.out.println("Selected the following Item: " + item);
			
			BigObjectCustomFieldDialog.show(stage, fields, item);
		}
	}
	
	void eventDeleteField() {
		ObservableList<BigObjectCustomField> items = fields.getItems();
		BigObjectCustomField item;
		
		item = fields.getSelectionModel().getSelectedItem();
		
		if (items != null && item != null) {
			items.remove(item);
		}
	}
	
	void eventAddIndex() {
		ObservableList<Index> items = indexes.getItems();
		
		if (items == null) {
			items = FXCollections.observableList(new ArrayList<Index>());
		}
		
		items.add(new Index());
		
		indexes.setItems(items);
	}
	
	void eventEditIndex() {
		Index item;
		
		item = indexes.getSelectionModel().getSelectedItem();
		
		if (item != null) {
			System.out.println("Selected the following Item: " + item);
			
			BigObjectIndexDialog.show(stage, indexes, item);
		}
	}
	
	void eventDeleteIndex() {
		ObservableList<Index> items = indexes.getItems();
		Index item;
		
		item = indexes.getSelectionModel().getSelectedItem();
		
		if (items != null && item != null) {
			items.remove(item);
		}
	}
}
