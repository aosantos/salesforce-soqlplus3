package com.app.soqlplus3.dialog;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import com.app.soqlplus3.Util;
import javafx.scene.control.Alert.AlertType;

public class ExceptionDialog {
	public static void showDialog(String title, String header, String body, Throwable t, String buttonLabel) {
		Alert alert = new Alert(AlertType.ERROR);
		ButtonType yesButton = new ButtonType(buttonLabel);
		String exceptionText = Util.getStackTraceString(t);
		Label exceptionLabel = new Label("Stacktrace:");
		TextArea exceptionTextArea = new TextArea(exceptionText);
		GridPane exceptionGrid = new GridPane();
		
		exceptionTextArea.setEditable(false);
		exceptionTextArea.setWrapText(true);
		exceptionTextArea.setMaxWidth(Double.MAX_VALUE);
		exceptionTextArea.setMaxHeight(Double.MAX_VALUE);
		
		GridPane.setVgrow(exceptionTextArea, Priority.ALWAYS);
		GridPane.setHgrow(exceptionTextArea, Priority.ALWAYS);
		
		exceptionGrid.setMaxWidth(Double.MAX_VALUE);
		exceptionGrid.add(exceptionLabel, 0, 0);
		exceptionGrid.add(exceptionTextArea, 0, 1);
		
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(body);
		alert.getButtonTypes().setAll(yesButton);
		alert.getDialogPane().setExpandableContent(exceptionGrid);
		
		alert.showAndWait();
	}
}
