package com.app.soqlplus3.dialog;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;

public class ConfirmationDialog {
	public static String showDialog(String title, String header, String body, String yes, String no) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		ButtonType yesButton = new ButtonType(yes);
		ButtonType noButton = new ButtonType(no);
		Optional<ButtonType> alertResult;
		
		String result;
		
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(body);

		alert.getButtonTypes().clear();
		
		if (yes != null) {
			alert.getButtonTypes().add(yesButton);
		}
		
		if (no != null) {
			alert.getButtonTypes().add(noButton);
		}
		
		alertResult = alert.showAndWait();
		
		if (alertResult.get() == yesButton) {
			result = yes;
		} else if (alertResult.get() == noButton) {
			result = no;
		} else {
			result = null;
		}
		
		return result;
	}
}
