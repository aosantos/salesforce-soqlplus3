package com.app.soqlplus3.dialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import com.app.soqlplus3.Connections;
import com.app.soqlplus3.Salesforce;

public class SalesforceConnectionDialog {
	final static int SCENE_WIDTH = 430;
	final static int SCENE_HEIGHT = 220;

	final static String DIALOG_TITLE = "Connect to Salesforce.com";
	
	final static String SALESFORCE_COM = "salesforce.com";

	final static String LOGIN_PRODUCTION = "login";
	final static String LOGIN_SANDBOX = "test";

	final static String API_LOCATION = "/services/Soap/u/";
	final static String API_VERSION = "37.0";
	
	final static Insets INSETS = new Insets(8);

	enum DialogMode {
		Connect, Edit
	};
	
	Salesforce connection;

	Connections configuration;
	
	DialogMode dialogMode;
	
	/* JavaFX Controls */
	Stage parentStage;
	Stage stage;
	Scene scene;

	Button buttonNewConnection;
	Button buttonConnect;
	Button buttonEdit;
	Button buttonSave;
	Button buttonCancel;
	Button buttonDelete;

	GridPane grid;

	ComboBox<String> comboBoxConnectionName;
	TextField connectionName;
	TextField loginServer;
	TextField username;
	PasswordField password;
	PasswordField token;
	Text errorMessage;
	
	Label labelConnectionName = new Label("Connection Name");
	Label labelLoginServer = new Label("Login Server");
	Label labelUsername = new Label("Username");
	Label labelPassword = new Label("Password");
	Label labelToken = new Label("Token");

	// Old version of record being edited
	Map<String,String> oldVersion = new HashMap<String,String>();
	
	/**
	 * Constructor.
	 * 
	 */
	private SalesforceConnectionDialog() {
		dialogMode = DialogMode.Connect;
		
		initControls();
		initButtonHandlers();
		initControlEvents();
	}

	private void initControls() {
		buttonNewConnection = new Button("New Connection");
		buttonConnect = new Button("Connect");
		buttonEdit = new Button("Edit");
		buttonSave = new Button("Save");
		buttonCancel = new Button("Cancel");
		buttonDelete = new Button("Delete");

		comboBoxConnectionName = new ComboBox<String>();
		connectionName = new TextField();
		loginServer = new TextField();
		username = new TextField();
		password = new PasswordField();
		token = new PasswordField();
		errorMessage = new Text();
	}

	private void initButtonHandlers() {
		buttonNewConnection.setOnAction((event) -> {
			eventNewConnection();
		});
		buttonConnect.setOnAction((event) -> {
			eventConnect();
		});
		buttonEdit.setOnAction((event) -> {
			eventEdit(comboBoxConnectionName.getValue());
		});
		buttonSave.setOnAction((event) -> {
			eventSave();
		});
		buttonCancel.setOnAction((event) -> {
			eventCancel();
		});
		buttonDelete.setOnAction((event) -> {
			eventDelete();
		});
	}

	private void initControlEvents() {
		comboBoxConnectionName.valueProperty().addListener(
			(observable, oldValue, newValue) -> {
				eventConnectionNameChanged(newValue);
			}
		);
	}
	
	/**
	 * Get the Salesforce Connection.
	 * 
	 * @return
	 */
	public Salesforce getConnection() {
		return connection;
	}
	
	/**
	 * Get the username for the connection.
	 * 
	 * @return
	 */
	public String getUsername() {
		return username.getText();
	}
	
	/**
	 * Get the connection name.
	 * 
	 * @return
	 */
	public String getConnectionName() {
		return comboBoxConnectionName.getValue();
	}
	
	/**
	 * Get the login server.
	 * 
	 * @return
	 */
	public String getLoginServer() {
		return loginServer.getText();
	}
	
	/**
	 * Has connection been established?
	 * 
	 * @return
	 */
	public Boolean isConnected() {
		if (connection != null && connection.isConnected) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Create a new Connection.
	 * 
	 */
	private void eventNewConnection() {
		dialogMode = DialogMode.Edit;
		
		oldVersion.put("connectionName", null);
		oldVersion.put("loginServer", null);
		oldVersion.put("username", null);
		oldVersion.put("password", null);
		oldVersion.put("token", null);
		
		connectionName.setText("");
		loginServer.setText("https://" + LOGIN_PRODUCTION + "." + SALESFORCE_COM + API_LOCATION + API_VERSION);
		username.setText("");
		password.setText("");
		token.setText("");
		
		refreshState();
	}

	/**
	 * Connect to Salesforce.
	 * 
	 */
	private void eventConnect() {
		connection = new Salesforce();
		
		try {
			connection.getConnection(loginServer.getText(), username.getText(), password.getText(), token.getText());
			
			stage.close();
		} catch (Throwable t) {
			ExceptionDialog.showDialog(
					"Exception Caught", 
					"An exception was caught in the application.",
					"Please see the Stack Trace dump for the Exception that was caught.",
					t,
					"continue"
			);		
		}
	}

	/**
	 * Edit the Connection.
	 * 
	 */
	private void eventEdit(String alias) {
		dialogMode = DialogMode.Edit;
		
		oldVersion.put("connectionName", connectionName.getText());
		oldVersion.put("loginServer", loginServer.getText());
		oldVersion.put("username", username.getText());
		oldVersion.put("password", password.getText());
		oldVersion.put("token", token.getText());
		
		refreshState();
	}

	/**
	 * Save the Connection(s).
	 * 
	 */
	private void eventSave() {
		String oldAlias = oldVersion.get("connectionName");
		String newAlias = connectionName.getText();

		String saveLoginServer;
		String saveUsername;
		String savePassword;
		String saveToken;

		System.out.println("eventSave");

		if (! fieldValidation()) {
			System.out.println("eventSave - returning");

			return;
		}

		System.out.println("eventSave - before try");

		System.out.println("eventSave ---> loginServer=" + loginServer.getText() + ", username=" + username.getText() + ", password=" + password.getText() + ", token=" + token.getText());
		
		saveLoginServer = loginServer.getText();
		saveUsername = username.getText();
		savePassword = password.getText();
		saveToken = token.getText();

		try {
			System.out.println("eventSave - oldAlias = " + oldAlias + ", newAlias = " + newAlias);

			if (oldAlias != null && newAlias != null && oldAlias.equals(newAlias) == false) {
				// Alias change
				comboBoxConnectionName.getItems().remove(oldAlias);
				comboBoxConnectionName.getItems().add(newAlias);
				
				Collections.sort(comboBoxConnectionName.getItems());
				
				System.out.println("eventSave - replace connection");

				configuration.replaceConnection(oldAlias, newAlias, loginServer.getText(), username.getText(), password.getText(), token.getText());
			} else if (oldAlias == null || oldAlias != null && newAlias != null && oldAlias.equals(newAlias)) {
				// Alias creation / update

				comboBoxConnectionName.getItems().add(newAlias);
				
				Collections.sort(comboBoxConnectionName.getItems());

				System.out.println("eventSave - set connection ---- username = " + username.getText());
				
				configuration.setConnection(newAlias, saveLoginServer, saveUsername, savePassword, saveToken);
			}
		} catch (Throwable t) {
			ExceptionDialog.showDialog(
					"Exception Caught", 
					"An exception was caught in the application.",
					"Please see the Stack Trace dump for the Exception that was caught.",
					t,
					"continue"
			);		
		}
		
		dialogMode = DialogMode.Connect;
		
		errorMessage.setText("");
		
		comboBoxConnectionName.setValue(newAlias);
		
		refreshState();
	}
	
	/**
	 * Cancel Connect.
	 * 
	 */
	private void eventCancel() {
		dialogMode = DialogMode.Connect;
		
		// Revert fields to old values
		connectionName.setText(oldVersion.get("connectionName"));
		loginServer.setText(oldVersion.get("loginServer"));
		username.setText(oldVersion.get("username"));
		password.setText(oldVersion.get("password"));
		token.setText(oldVersion.get("token"));
		
		errorMessage.setText("");
		
		refreshState();
	}

	/**
	 * Delete a Connection.
	 * 
	 */
	private void eventDelete() {
		String alias = comboBoxConnectionName.getValue();
		
		dialogMode = DialogMode.Connect;
		
		if (alias != null) {
			try {
				configuration.deleteConnection(alias);
				
				comboBoxConnectionName.getItems().remove(alias);
			} catch (Throwable t) {
				ExceptionDialog.showDialog(
						"Exception Caught", 
						"An exception was caught in the application.",
						"Please see the Stack Trace dump for the Exception that was caught.",
						t,
						"continue"
				);		
			}
		}
		
		comboBoxConnectionName.setValue("");
		loginServer.setText("");
		username.setText("");
		password.setText("");
		token.setText("");
		
		errorMessage.setText("");
		
		refreshState();
	}
	
	/**
	 * Selected a Connection Name.
	 * 
	 * @param connectionName
	 */
	private void eventConnectionNameChanged(String alias) {
		Map<String,String> params;
		String paramLoginServer = null;
		String paramUsername = null;
		String paramPassword = null;
		String paramToken = null;
		
		//System.out.println("Set connectionName: " + alias);
		
		if (connectionName != null) {
			params = configuration.getConnection(alias);
			
			//System.out.println("Got the following params for the alias " + alias + ": " + params.toString());
			
			if (params != null) {
				paramLoginServer = params.get("loginServer");
				paramUsername = params.get("username");
				paramPassword = params.get("password");
				paramToken = params.get("securityToken");
			}
		}
		
		if (paramLoginServer == null) {
			paramLoginServer = "";
		}
		
		if (paramUsername == null) {
			paramUsername = "";
		}
		
		if (paramPassword == null) {
			paramPassword = "";
		}
		
		if (paramToken == null) {
			paramToken = "";
		}
		
		// Set on the UI
		loginServer.setText(paramLoginServer);
		username.setText(paramUsername);
		password.setText(paramPassword);
		token.setText(paramToken);

		connectionName.setText(alias);
		
		refreshState();
	}
	
	/**
	 * Validate the correctness of inputs.
	 * 
	 * @return true if all fields are correct, else return false.
	 * 
	 */
	private Boolean fieldValidation() {
		String connectionName = this.connectionName.getText();
		String loginServer = this.loginServer.getText();
		String username = this.username.getText();
		
		/*
		String password = this.password.getText();
		String token = this.token.getText();
		*/
		
		if (isBlank(connectionName)) {
			errorMessage.setText("Connection Name is mandatory");
			
			return false;
		}
		
		if (isBlank(loginServer)) {
			errorMessage.setText("Login Server is mandatory");
			
			return false;
		}
		
		if (isBlank(username)) {
			errorMessage.setText("Username is mandatory");
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * Check if @str is null, empty or filled with blanks.
	 * 
	 * @param str
	 * @return
	 */
	private Boolean isBlank(String str) {
		if (str == null || str != null && str.trim().isEmpty() == true) {
			return true;
		}
		
		return false;
	}

	
	/**
	 * 
	 * @param parentStage
	 */
	private void setStage(Stage parentStage) {
		this.parentStage = parentStage;
		
		// Build and Show the UI.
		stage = new Stage();
		
		stage.setTitle(DIALOG_TITLE);
		stage.initModality(Modality.WINDOW_MODAL);
		stage.initOwner(parentStage);
		
		grid = new GridPane();
		grid.setPadding(INSETS);
		
		initDialogContent(grid);
		
		scene = new Scene(grid, SCENE_WIDTH, SCENE_HEIGHT);
		
		stage.setScene(scene);
		stage.showAndWait();
	}
	
	/**
	 * Initialize the Dialog Content.
	 * 
	 * @param grid
	 */
	private void initDialogContent(GridPane grid) {
		HBox bottomButtonsBox = new HBox();
		
		// Adjust the Elements by adjusting the labels.
		adjustLabel(labelConnectionName);
		adjustLabel(labelLoginServer);
		adjustLabel(labelPassword);
		adjustLabel(labelToken);
		adjustLabel(labelUsername);
		
		bottomButtonsBox.setAlignment(Pos.CENTER);
		
		bottomButtonsBox.getChildren().addAll(
				buttonNewConnection,
				buttonConnect,
				buttonEdit,
				buttonSave,
				buttonCancel,
				buttonDelete
		);
		
		// Labels
		grid.add(labelConnectionName, 0,  0);
		grid.add(labelLoginServer, 0,  1);
		grid.add(labelUsername, 0,  2);
		grid.add(labelPassword, 0,  3);
		grid.add(labelToken, 0,  4);
		
		// Controls
		grid.add(comboBoxConnectionName, 1,  0);
		grid.add(connectionName, 1, 0);
		
		connectionName.setVisible(false);
		
		grid.add(loginServer, 1,  1);
		grid.add(username, 1,  2);
		grid.add(password, 1,  3);
		grid.add(token, 1,  4);
		
		// Bottom Buttons
		grid.add(bottomButtonsBox, 0, 5, 2, 1);
		
		// Error Message Placeholder
		grid.add(errorMessage, 0, 6, 2, 1);
		
		GridPane.setHalignment(errorMessage, HPos.CENTER);
		
		errorMessage.setFill(Color.RED);
		errorMessage.setTextAlignment(TextAlignment.CENTER);
		
		
		
		refreshState();
	}
	
	/**
	 * Refresh the Dialog State.
	 * 
	 */
	private void refreshState() {
		setInputState();
		setButtonState();
	}
	
	/**
	 * Enable / Disable the Inputs depending on current context.
	 * 
	 */
	private void setInputState() {
		if (comboBoxConnectionName.getItems().isEmpty()) {
			// Refresh the Connection Name ComboBox
			ObservableList<String> connectionNameList = FXCollections.observableArrayList(getConnectionNameList());
		
			comboBoxConnectionName.getItems().addAll(connectionNameList);
		}
		
		if (dialogMode == DialogMode.Connect) {
			// Read Only
			comboBoxConnectionName.setDisable(false);
			connectionName.setDisable(true);
			loginServer.setDisable(true);
			username.setDisable(true);
			password.setDisable(true);
			token.setDisable(true);
			
			comboBoxConnectionName.setVisible(true);
			connectionName.setVisible(false);
		} else if (dialogMode == DialogMode.Edit) {
			// Editable
			comboBoxConnectionName.setDisable(true);
			connectionName.setDisable(false);
			loginServer.setDisable(false);
			username.setDisable(false);
			password.setDisable(false);
			token.setDisable(false);
			
			comboBoxConnectionName.setVisible(false);
			connectionName.setVisible(true);
		}
	}
	
	/**
	 * Enable / Disable the buttons depending on current context.
	 * 
	 */
	private void setButtonState() {
		if (dialogMode == DialogMode.Connect) {
			Boolean isConnectionNameAvailable = connectionName.getText() != null && connectionName.getText().isEmpty() == false;
			
			buttonNewConnection.setDisable(false);
			buttonConnect.setDisable(! isConnectionNameAvailable);
			buttonEdit.setDisable(! isConnectionNameAvailable); // TODO: depends on any connection selected
			buttonSave.setDisable(true);
			buttonCancel.setDisable(true);
			buttonDelete.setDisable(true);
		} else if (dialogMode == DialogMode.Edit) {
			buttonNewConnection.setDisable(true);
			buttonConnect.setDisable(true);
			buttonEdit.setDisable(true);
			buttonSave.setDisable(false);
			buttonCancel.setDisable(false);
			buttonDelete.setDisable(false);			
		}
	}
	
	/**
	 * Adjust Padding, Preferred Width and Alignment of Labels.
	 * 
	 * @param label
	 */
	private void adjustLabel(Label label) {
		label.setPadding(INSETS);
		label.setAlignment(Pos.CENTER_RIGHT);
	}
	
	/**
	 * Show the Dialog and return the Connected Salesforce Instance.
	 * 
	 * @param parentStage
	 * @return
	 */
	public static SalesforceConnectionDialog showConnectDialog(Stage parentStage) {
		SalesforceConnectionDialog dialog = new SalesforceConnectionDialog();
		
		dialog.setStage(parentStage);
		
		return dialog;
	}
	
	private List<String> getConnectionNameList() {
		List<String> resultConnectionNameList = new ArrayList<String>();
		
		try {
			configuration = new Connections();
		} catch (Throwable t) {
			ExceptionDialog.showDialog(
					"Exception Caught", 
					"An exception was caught in the application.",
					"Please see the Stack Trace dump for the Exception that was caught.",
					t,
					"continue"
			);			
		}
		
		if (configuration != null) {
			List<String> aliases = configuration.getAliases();
			
			Collections.sort(aliases);
			
			if (aliases != null && ! aliases.isEmpty()) {
				resultConnectionNameList.addAll(aliases);
			}
		}
		
		return resultConnectionNameList;
	}
}
