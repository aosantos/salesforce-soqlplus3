package com.app.soqlplus3.dialog;

import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import com.app.soqlplus3.Util;
import com.app.soqlplus3.model.Index;

public class BigObjectIndexDialog {
	final static int WIDTH = 400;
	final static int HEIGHT = 300;
	final static String TITLE = "Edit Big Object Index";
	final static Insets INSETS = new Insets(8);
	
	// JavaFX Controls
	Stage parentStage;
	Stage stage;
	Scene scene;
	
	Button saveButton;
	Button cancelButton;
	
	GridPane grid;
	
	TextField name = new TextField();
	ComboBox<String> sortDirection = new ComboBox<>(
			FXCollections.observableArrayList(
					Index.SortDirection.ASC.name(),
					Index.SortDirection.DESC.name()
			)
	);

	Index index;
	
	TableView<Index> table;
	
	public BigObjectIndexDialog() {
		initControls();
		initButtonHandlers();
	}
	
	void initControls() {
		saveButton = new Button("Save");
		cancelButton = new Button("Cancel");		
	}
	
	void initButtonHandlers() {
		saveButton.setOnAction((event) -> { eventSave(); });
		cancelButton.setOnAction((event) -> { eventCancel(); });
	}
	
	void eventSave() {
		index.name = name.getText();
		
		if (Util.isEmpty(sortDirection.getValue()) == false) {
			index.sortDirection = Index.SortDirection.valueOf(sortDirection.getValue());
		} else {
			index.sortDirection = null;
		}
		
		table.refresh();
		
		stage.close();
	}
	
	void eventCancel() {
		stage.close();
	}
	
	public static BigObjectIndexDialog show(Stage parentStage, TableView<Index> table, Index index) {
		BigObjectIndexDialog dialog = new BigObjectIndexDialog();
		
		dialog.table = table;
		
		dialog.setIndex(index);
		
		dialog.setStage(parentStage);
		
		return dialog;
	}
	
	void setStage(Stage parentStage) {
		this.parentStage = parentStage;
		
		stage = new Stage();
		
		stage.setTitle(TITLE);
		stage.initModality(Modality.WINDOW_MODAL);
		stage.initOwner(parentStage);
		
		grid = new GridPane();
		grid.setPadding(INSETS);
		
		initContent(grid);
		
		scene = new Scene(grid, WIDTH, HEIGHT);
		
		stage.setScene(scene);
		stage.showAndWait();
	}
	
	void initContent(GridPane grid) {
		HBox buttons = new HBox();
		
		buttons.setAlignment(Pos.CENTER);
		
		buttons.getChildren().addAll(saveButton, cancelButton);
		
		grid.add(new Label("Name"), 0, 0);
		grid.add(new Label("Sort Direction"), 0, 1);
		grid.add(buttons, 0, 2, 2, 1);
		
		grid.add(name, 1, 0);
		grid.add(sortDirection, 1, 1);
	}
	
	void setIndex(Index index) {
		this.index = index;
		
		name.setText(index.name);
		sortDirection.setValue(index.sortDirection.name());
	}
}
