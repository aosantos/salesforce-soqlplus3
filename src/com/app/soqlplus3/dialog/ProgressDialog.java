package com.app.soqlplus3.dialog;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import com.app.soqlplus3.Application.SalesforceAsyncQuery;

public class ProgressDialog {
	final static int WIDTH = 600;
	final static int HEIGHT = 180;
	final static Insets INSETS = new Insets(8);
	
	Stage parentStage;
	Stage stage;
	Scene scene;
	
	Button okButton;
	Button cancelButton;
	
	Text bodyText;
	Text feedbackText;
	
	SalesforceAsyncQuery asyncQuery;
	
	public ProgressBar progressBar;
	
	String feedback;
	String body;
	int minValue;
	int maxValue;
	int currentValue;
	
	boolean hasOkButton;
	boolean hasCancelButton;
	
	public Stage getStage() {
		return stage;
	}
	
	public void setFinished(boolean finished) {
		
	}
	
	public void setCanceled(boolean canceled) {
		
	}
	
	public void setHasOkButton(boolean okButton, String okText) {
		hasOkButton = okButton;
		
		if (hasOkButton) {
			this.okButton = new Button(okText);
		}
	}
	
	public void setHasCancelButton(boolean cancelButton, String cancelText) {
		hasCancelButton = cancelButton;
		
		if (hasCancelButton) {
			this.cancelButton = new Button(cancelText);
		}
	}
	
	public void setProgress() {
		if (maxValue - minValue != 0 && progressBar != null) {
			int progress = (currentValue - minValue) / (maxValue - minValue);
		
			progressBar.setProgress(progress);
		}
	}
	
	public void setFeedbackText(Number newValue, Integer totalRecords) {
		if (totalRecords == null) {
			feedbackText.setText("calculating ...");
		} else {
			Integer fetched;
			
			fetched = new Integer((int)(newValue.doubleValue() * totalRecords));
			
			feedbackText.setText(
				fetched.intValue() + " of " + totalRecords + " Records"
			);
		}
	}
	
	public static ProgressDialog getDialog(
			SalesforceAsyncQuery asyncQuery,
			Stage parentStage,
			Modality modality,
			String title,
			String body,
			boolean hasOkButton,
			String okButtonText,
			boolean hasCancelButton,
			String cancelButtonText
	) {
		ProgressDialog dialog = new ProgressDialog();
		
		dialog.asyncQuery = asyncQuery;
		
		dialog.setStage(parentStage, modality, title, body, hasOkButton, okButtonText, hasCancelButton, cancelButtonText);
		
		return dialog;
	}
	
	public void setStage(Stage parentStage, Modality modality, String title, String body, boolean hasOkButton, String okButtonText, boolean hasCancelButton, String cancelButtonText) {
		this.parentStage = parentStage;
		
		this.body = body;

		setHasOkButton(hasOkButton, okButtonText);
		setHasCancelButton(hasCancelButton, cancelButtonText);
		
		stage = new Stage();
		
		stage.setTitle(title);
		stage.initModality(modality);
		stage.initOwner(parentStage);
		
		stage.setScene(getScene());
		
		setProgress();
		
		/*
		stage.show();
		stage.toFront();
		*/
	}
	
	public Scene getScene() {
		VBox verticalBox = new VBox();
		HBox horizontalBox = new HBox();
		HBox horizontalProgressBox = new HBox();
		
		Scene scene = new Scene(verticalBox);
		
		verticalBox.setPrefHeight(HEIGHT - 32);
		verticalBox.setMaxHeight(HEIGHT - 32);
		verticalBox.setPadding(INSETS);
		verticalBox.setAlignment(Pos.TOP_CENTER);
		
		horizontalBox.setAlignment(Pos.CENTER);
		
		horizontalProgressBox.setAlignment(Pos.CENTER);
		
		bodyText = new Text(body);
		feedbackText = new Text("calculating ...");
		progressBar = new ProgressBar();
		progressBar.setPadding(INSETS);
		progressBar.setPrefWidth(WIDTH / 2);
		
		horizontalProgressBox.getChildren().addAll(new Text("0%"), progressBar, new Text("100%"));
		
		//verticalBox.getChildren().addAll(bodyText, feedbackText, progressBar);
		verticalBox.getChildren().addAll(bodyText, feedbackText, horizontalProgressBox);
		
		if (hasOkButton) {
			horizontalBox.getChildren().add(okButton);
		}
		
		if (hasCancelButton) {
			horizontalBox.getChildren().add(cancelButton);
			
			cancelButton.setOnAction((event) -> {
				try {
					asyncQuery.task.cancel();
				} catch (Throwable t) {
					t.printStackTrace();
				}
			});
		}
		
		if (hasOkButton || hasCancelButton) {
			verticalBox.getChildren().addAll(horizontalBox);
		}
		
		return scene;
	}
}
