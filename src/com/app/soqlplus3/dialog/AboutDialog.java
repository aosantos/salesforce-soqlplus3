package com.app.soqlplus3.dialog;

import java.util.ArrayList;
import java.util.List;

import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import com.app.soqlplus3.Application;

public class AboutDialog {
	final static int WIDTH = 400;
	final static int HEIGHT = 300;
	final static String TITLE = "About";
	final static Insets INSETS = new Insets(8);
	
	final static String[] aboutText = new String[] {
			Application.APPLICATION_TITLE,
			"",
			"(c) Copyright 2017 - 2022 Undisclosed",
			"",
			"Contact Address: sevens.estate-0i@icloud.com",
			"",
			"History",
			"\t2018-06-30, Fixed the Progress Dialog to use the Total Reccords taking in consideration the SOQL where clause",
			""
	};
	
	Stage parentStage;
	Stage stage;
	Scene scene;
	
	Button okButton;
	
	VBox scrollingArea;
	
	GridPane grid;
	
	public AboutDialog() {
		initControls();
		initButtonHandlers();
	}
	
	void initControls() {
		okButton = new Button("OK");
	}
	
	void initButtonHandlers() {
		okButton.setOnAction((event) -> { eventOK(); });
	}
	
	void eventOK() {
		stage.close();
	}
	
	public static AboutDialog show(Stage parentStage) {
		AboutDialog aboutDialog = new AboutDialog();
		
		aboutDialog.setStage(parentStage);
		
		return aboutDialog;
	}
		
	void setStage(Stage parentStage) {
		List<String> list = new ArrayList<>();
		VBox vbox = new VBox();
		
		this.parentStage = parentStage;
		
		for (String aboutTextItem : aboutText) {
			list.add(aboutTextItem);
		}
		
		scrollingArea = new VBox();

		scrollingArea.setPrefHeight(HEIGHT - 32);
		scrollingArea.setMaxHeight(HEIGHT - 32);
		scrollingArea.setPadding(INSETS);
		
		vbox.getChildren().addAll(scrollingArea, buttons());
		
		stage = new Stage();
		stage.setTitle(TITLE);
		stage.initModality(Modality.WINDOW_MODAL);
		stage.initOwner(parentStage);;
		
		stage.setScene(new Scene(vbox));

		verticalScrollText(scrollingArea, list);
		
		stage.showAndWait();
	}
	
	private void verticalScrollText(VBox parent, List<String> textList) {
		Text text = new Text(String.join("\n", textList));
		
		parent.getChildren().add(text);
		
		text.setLayoutX(0);
		text.setLayoutY(20);
		
		TranslateTransition tt = new TranslateTransition(Duration.millis(20000), text);
		
		tt.setFromY(HEIGHT);
		tt.setToY(-HEIGHT * 2);
		
		tt.setCycleCount(Timeline.INDEFINITE);
		tt.setAutoReverse(false);
		
		tt.play();
	}
	
	private HBox buttons() {
		HBox hbox = new HBox();
		
		hbox.setAlignment(Pos.CENTER);
		hbox.getChildren().add(okButton);
		
		return hbox;
	}
}
