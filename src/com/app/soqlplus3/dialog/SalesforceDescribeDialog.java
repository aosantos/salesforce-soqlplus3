package com.app.soqlplus3.dialog;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import com.app.soqlplus3.Salesforce;
import com.app.soqlplus3.Salesforce.FieldDescribe;
import com.app.soqlplus3.Salesforce.ObjectDescribe;

public class SalesforceDescribeDialog {
	final static int SCENE_WIDTH = 800;
	final static int SCENE_HEIGHT = 500;
	final static int MAX_SCENE_WIDTH = 1300;
	final static int MAX_SCENE_HEIGHT = 1300;
	
	final static String DIALOG_TITLE = "Describe Object";
	
	final static Insets INSETS = new Insets(8);

	static Integer staticRowNum = 0;
	
	class DescribeRecord {
		public FieldDescribe fieldDescribe;
		public Integer rowNum;
		
		public DescribeRecord(FieldDescribe fieldDescribe) {
			this.fieldDescribe = fieldDescribe;
			
			rowNum = ++staticRowNum;
		}
	}
	
	TableView<DescribeRecord> tableView = new TableView<DescribeRecord>();
	
	/* JavaFX Controls */
	Stage parentStage;
	Stage stage;
	Scene scene;
	
	Button buttonDescribe;
	
	GridPane grid;
	TextField objectName;
	Text errorMessage;
	
	Label labelObjectName = new Label("Object Name");
	Label labelObjectDescribe = new Label("Object Describe");

	Salesforce salesforce;
	
	/**
	 * Constructor.
	 * 
	 */
	private SalesforceDescribeDialog() {
		initControls();
		initButtonHandlers();
		initControlEvents();
	}
	
	private void initControls() {
		buttonDescribe = new Button("Describe");
		
		objectName = new TextField();
		
		errorMessage = new Text();
		
		tableView.setPrefWidth(MAX_SCENE_WIDTH);
		tableView.setPrefHeight(MAX_SCENE_HEIGHT);
	}
	
	private void initButtonHandlers() {
		buttonDescribe.setOnAction((event) -> { eventDescribe(); });
	}
	
	private void initControlEvents() {
		
	}
	
	@SuppressWarnings("unchecked")
	private void eventDescribe() {
		try {
			if (! fieldValidation()) {
				return;
			}
			
			System.out.println("objectName: " + objectName.getText());
			System.out.println("salesforce: " + getSalesforce());
			
			ObjectDescribe describeResult = getSalesforce().describeObject(objectName.getText());
			
			//System.out.println(describeResult);
			
			if (describeResult != null) {				
				tableView.getColumns().clear();
				tableView.getItems().clear();
				
				// set the tableView columns
				TableColumn<DescribeRecord,Integer> tableColumnRowNum = new TableColumn<>("rownum");
				TableColumn<DescribeRecord,String> tableColumnLabel = new TableColumn<>("label");
				TableColumn<DescribeRecord,String> tableColumnField = new TableColumn<>("api name");
				TableColumn<DescribeRecord,String> tableColumnType = new TableColumn<>("type");
				TableColumn<DescribeRecord,String> tableColumnSalesforceType = new TableColumn<>("type");
				TableColumn<DescribeRecord,String> tableColumnMandatory = new TableColumn<>("mandatory?");
				TableColumn<DescribeRecord,String> tableColumnExtraTypeInfo = new TableColumn<>("extra type info");
				TableColumn<DescribeRecord,String> tableColumnIsAutoNumber = new TableColumn<>("autonumber?");
				TableColumn<DescribeRecord,String> tableColumnIsCaseSensitive = new TableColumn<>("case sensitive?");
				TableColumn<DescribeRecord,String> tableColumnIsExternalId = new TableColumn<>("external id?");
				TableColumn<DescribeRecord,String> tableColumnLength = new TableColumn<>("length");
				TableColumn<DescribeRecord,String> tableColumnDigits = new TableColumn<>("digits");
				TableColumn<DescribeRecord,String> tableColumnPrecision = new TableColumn<>("precision");
				TableColumn<DescribeRecord,String> tableColumnReferenceTo = new TableColumn<>("reference to");
				
				tableColumnRowNum.setCellValueFactory(
						new Callback<TableColumn.CellDataFeatures<DescribeRecord,Integer>, ObservableValue<Integer>>() {
							@Override
							public ObservableValue<Integer> call(TableColumn.CellDataFeatures<DescribeRecord, Integer> p) {
								return new SimpleObjectProperty<Integer>(p.getValue().rowNum);
							}
						}
				);
				
				tableColumnLabel.setCellValueFactory(
						new Callback<TableColumn.CellDataFeatures<DescribeRecord,String>, ObservableValue<String>>() {
							@Override
							public ObservableValue<String> call(TableColumn.CellDataFeatures<DescribeRecord, String> p) {
								return new SimpleStringProperty(String.valueOf(p.getValue().fieldDescribe.label));
							}
						}
				);
				
				tableColumnField.setCellValueFactory(
						new Callback<TableColumn.CellDataFeatures<DescribeRecord,String>, ObservableValue<String>>() {
							@Override
							public ObservableValue<String> call(TableColumn.CellDataFeatures<DescribeRecord, String> p) {
								return new SimpleStringProperty(String.valueOf(p.getValue().fieldDescribe.name));
							}
						}
				);
				
				tableColumnType.setCellValueFactory(
						new Callback<TableColumn.CellDataFeatures<DescribeRecord,String>, ObservableValue<String>>() {
							@Override
							public ObservableValue<String> call(TableColumn.CellDataFeatures<DescribeRecord, String> p) {
								return new SimpleStringProperty(String.valueOf(p.getValue().fieldDescribe.type));
							}
						}
				);

				tableColumnMandatory.setCellValueFactory(
						new Callback<TableColumn.CellDataFeatures<DescribeRecord,String>, ObservableValue<String>>() {
							@Override
							public ObservableValue<String> call(TableColumn.CellDataFeatures<DescribeRecord, String> p) {
								return new SimpleStringProperty(String.valueOf(p.getValue().fieldDescribe.isNillable ? "" : "yes"));
							}
						}
				);
				
				tableColumnExtraTypeInfo.setCellValueFactory(
						new Callback<TableColumn.CellDataFeatures<DescribeRecord,String>, ObservableValue<String>>() {
							@Override
							public ObservableValue<String> call(TableColumn.CellDataFeatures<DescribeRecord, String> p) {
								return new SimpleStringProperty(String.valueOf(p.getValue().fieldDescribe.extraTypeInfo));
							}
						}
				);
				
				tableColumnIsAutoNumber.setCellValueFactory(
						new Callback<TableColumn.CellDataFeatures<DescribeRecord,String>, ObservableValue<String>>() {
							@Override
							public ObservableValue<String> call(TableColumn.CellDataFeatures<DescribeRecord, String> p) {
								return new SimpleStringProperty(String.valueOf(p.getValue().fieldDescribe.isAutoNumber));
							}
						}
				);
				
				tableColumnIsCaseSensitive.setCellValueFactory(
						new Callback<TableColumn.CellDataFeatures<DescribeRecord,String>, ObservableValue<String>>() {
							@Override
							public ObservableValue<String> call(TableColumn.CellDataFeatures<DescribeRecord, String> p) {
								return new SimpleStringProperty(String.valueOf(p.getValue().fieldDescribe.isCaseSensitive));
							}
						}
				);
				
				tableColumnIsExternalId.setCellValueFactory(
						new Callback<TableColumn.CellDataFeatures<DescribeRecord,String>, ObservableValue<String>>() {
							@Override
							public ObservableValue<String> call(TableColumn.CellDataFeatures<DescribeRecord, String> p) {
								return new SimpleStringProperty(String.valueOf(p.getValue().fieldDescribe.isExternalId));
							}
						}
				);
				
				tableColumnLength.setCellValueFactory(
						new Callback<TableColumn.CellDataFeatures<DescribeRecord,String>, ObservableValue<String>>() {
							@Override
							public ObservableValue<String> call(TableColumn.CellDataFeatures<DescribeRecord, String> p) {
								return new SimpleStringProperty(String.valueOf(p.getValue().fieldDescribe.length));
							}
						}
				);
				
				tableColumnDigits.setCellValueFactory(
						new Callback<TableColumn.CellDataFeatures<DescribeRecord,String>, ObservableValue<String>>() {
							@Override
							public ObservableValue<String> call(TableColumn.CellDataFeatures<DescribeRecord, String> p) {
								return new SimpleStringProperty(String.valueOf(p.getValue().fieldDescribe.digits));
							}
						}
				);
				
				tableColumnPrecision.setCellValueFactory(
						new Callback<TableColumn.CellDataFeatures<DescribeRecord,String>, ObservableValue<String>>() {
							@Override
							public ObservableValue<String> call(TableColumn.CellDataFeatures<DescribeRecord, String> p) {
								return new SimpleStringProperty(String.valueOf(p.getValue().fieldDescribe.precision));
							}
						}
				);
				
				tableColumnReferenceTo.setCellValueFactory(
						new Callback<TableColumn.CellDataFeatures<DescribeRecord,String>, ObservableValue<String>>() {
							@Override
							public ObservableValue<String> call(TableColumn.CellDataFeatures<DescribeRecord, String> p) {
								return new SimpleStringProperty(String.valueOf(p.getValue().fieldDescribe.referenceTo));
							}
						}
				);
				
				tableColumnSalesforceType.setCellValueFactory(
						new Callback<TableColumn.CellDataFeatures<DescribeRecord,String>, ObservableValue<String>>() {
							@Override
							public ObservableValue<String> call(TableColumn.CellDataFeatures<DescribeRecord, String> p) {
								return new SimpleStringProperty(String.valueOf(p.getValue().fieldDescribe.salesforceType()));
							}
						}
				);

				
				tableView.getColumns().addAll(
						tableColumnRowNum,
						tableColumnLabel,
						tableColumnField,
						tableColumnSalesforceType,
						tableColumnMandatory/*,
						tableColumnLength,
						tableColumnDigits,
						tableColumnPrecision,
						tableColumnExtraTypeInfo,
						tableColumnIsAutoNumber,
						tableColumnIsCaseSensitive,
						tableColumnIsExternalId,
						tableColumnReferenceTo
 */
				);
				
				
				tableView.setItems(FXCollections.observableList(getDescribeRecords(describeResult)));
			}
		} catch (Throwable t) {
			ExceptionDialog.showDialog(
					"Exception Caught", 
					"An exception was caught in the application.",
					"Please see the Stack Trace dump for the Exception that was caught.",
					t,
					"continue"
			);	
		}
		
		refreshState();
	}
	
	public List<DescribeRecord> getDescribeRecords(ObjectDescribe describeResult) {
		List<DescribeRecord> result = new ArrayList<>();
		List<FieldDescribe> fields = describeResult.fieldList;
		
		staticRowNum = 0;
		
		for (FieldDescribe field : fields) {
			DescribeRecord describeRecord = new DescribeRecord(field);
			
			result.add(describeRecord);
		}
		
		return result;
	}
	
	private Boolean fieldValidation() {
		String objectName = this.objectName.getText();
		
		if (isBlank(objectName)) {
			errorMessage.setText("Object Name is mandatory");
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * Check if @str is null, empty or filled with blanks.
	 * 
	 * @param str
	 * @return
	 */
	private Boolean isBlank(String str) {
		if (str == null || str != null && str.trim().isEmpty() == true) {
			return true;
		}
		
		return false;
	}
	
	private void refreshState() {
		
	}
	
	private void adjustLabel(Label label) {
		label.setPadding(INSETS);
		label.setAlignment(Pos.CENTER_RIGHT);
	}
	
	private void setStage(Stage parentStage) {
		this.parentStage = parentStage;
		
		stage = new Stage();
		
		stage.setTitle(DIALOG_TITLE);
		//stage.initModality(Modality.WINDOW_MODAL);
		stage.initModality(Modality.NONE);
		stage.initOwner(parentStage);
		
		grid = new GridPane();
		grid.setPadding(INSETS);
		
		initDialogContent(grid);
		
		scene = new Scene(grid, SCENE_WIDTH, SCENE_HEIGHT);
		
		stage.setScene(scene);
		stage.showAndWait();
	}
	
	private void initDialogContent(GridPane grid) {
		HBox bottomButtonsBox = new HBox();
		
		adjustLabel(labelObjectName);
		
		bottomButtonsBox.setAlignment(Pos.CENTER);
		
		bottomButtonsBox.getChildren().addAll(buttonDescribe);
		
		grid.add(labelObjectName, 0, 0, 2, 1);
		grid.add(objectName, 0, 1);
		grid.add(labelObjectDescribe, 0, 2, 2, 1);
		grid.add(tableView, 0, 3, 2, 1);
		
		grid.add(bottomButtonsBox, 0, 4, 2, 1);
		
		grid.add(errorMessage, 0, 5, 2, 1);
		
		GridPane.setHalignment(errorMessage, HPos.CENTER);
		
		errorMessage.setFill(Color.RED);
		errorMessage.setTextAlignment(TextAlignment.CENTER);
		
		refreshState();
	}
	
	private void setSalesforce(Salesforce salesforce) {
		this.salesforce = salesforce;
	}
	
	private Salesforce getSalesforce() {
		return this.salesforce;
	}
	
	public static SalesforceDescribeDialog showDescribeDialog(Stage parentState, Salesforce salesforce) {
		SalesforceDescribeDialog dialog = new SalesforceDescribeDialog();
		
		dialog.setSalesforce(salesforce);
		
		dialog.setStage(parentState);
		
		return dialog;
	}
	
	public static SalesforceDescribeDialog showDescribeDialog(Stage parentState, Salesforce salesforce, String objectName) {
		SalesforceDescribeDialog dialog = new SalesforceDescribeDialog();
		
		dialog.setSalesforce(salesforce);
		dialog.objectName.setText(objectName);
		
		dialog.eventDescribe();
		
		dialog.setStage(parentState);
		
		return dialog;
	}
}
