package com.app.soqlplus3.dialog;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import com.app.soqlplus3.Util;
import com.app.soqlplus3.model.BigObjectCustomField;

public class BigObjectCustomFieldDialog {
	final static int WIDTH = 350;
	final static int HEIGHT = 350;
	final static String TITLE = "Edit Big Object Custom Field";
	final static Insets INSETS = new Insets(8);
	
	// JavaFX Controls
	Stage parentStage;
	Stage stage;
	Scene scene;
	
	Button saveButton;
	Button cancelButton;
	
	GridPane grid;
	
	TextField label = new TextField();
	TextField pluralLabel = new TextField();
	TextField fullName = new TextField();
	TextField referenceTo = new TextField();
	TextField relationshipName = new TextField();
	ComboBox<String> type = new ComboBox<>(
		FXCollections.observableArrayList(
			BigObjectCustomField.FieldType.Text.name(),
			BigObjectCustomField.FieldType.LongTextArea.name(),
			BigObjectCustomField.FieldType.Number.name(),
			BigObjectCustomField.FieldType.DateTime.name(),
			BigObjectCustomField.FieldType.Lookup.name()
			
		)
	);
	TextField scale = new TextField();
	TextField precision = new TextField();
	TextField length = new TextField();
	CheckBox required = new CheckBox();
	CheckBox externalId = new CheckBox();
	CheckBox unique = new CheckBox();
	
	BigObjectCustomField customField;
	
	TableView<BigObjectCustomField> table;
	
	public BigObjectCustomFieldDialog() {
		initControls();
		initButtonHandlers();
	}
	
	void initControls() {
		saveButton = new Button("Save");
		cancelButton = new Button("Cancel");
		
		scale.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (! newValue.matches("\\d*")) {
					scale.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});
		
		precision.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (! newValue.matches("\\d*")) {
					precision.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});
		
		length.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (! newValue.matches("\\d*")) {
					length.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});
	}
	
	void initButtonHandlers() {
		saveButton.setOnAction((event) -> { eventSave(); });
		cancelButton.setOnAction((event) -> { eventCancel(); });
	}
	
	void eventSave() {
		customField.fullName = fullName.getText();
		customField.label = label.getText();
		customField.pluralLabel = pluralLabel.getText();
		
		if (Util.isEmpty(length.getText()) == false) {
			customField.length = Integer.valueOf(length.getText());
		} else {
			customField.length = null;
		}
		
		if (Util.isEmpty(precision.getText()) == false) {
			customField.precision = Integer.valueOf(precision.getText());
		} else {
			customField.precision = null;
		}
		
		customField.referenceTo = referenceTo.getText();
		customField.relationshipName = relationshipName.getText();
		
		if (Util.isEmpty(scale.getText()) == false) {
			customField.scale = Integer.valueOf(scale.getText());
		} else {
			customField.scale = null;
		}
		
		if (Util.isEmpty(type.getValue()) == false) {
			customField.type = BigObjectCustomField.FieldType.valueOf(type.getValue());
		} else {
			customField.type = null;
		}
		
		customField.required = required.isSelected();
		customField.externalId = externalId.isSelected();
		customField.unique = unique.isSelected();
		
		table.refresh();
		
		stage.close();
	}
	
	void eventCancel() {
		stage.close();
	}
	
	public static BigObjectCustomFieldDialog show(Stage parentStage, TableView<BigObjectCustomField> table, BigObjectCustomField customField) {
		BigObjectCustomFieldDialog dialog = new BigObjectCustomFieldDialog();
		
		dialog.table = table;
		
		dialog.setCustomField(customField);
		
		dialog.setStage(parentStage);
		
		return dialog;
	}
	
	void setStage(Stage parentStage) {
		this.parentStage = parentStage;
		
		stage = new Stage();
		
		stage.setTitle(TITLE);
		stage.initModality(Modality.WINDOW_MODAL);
		stage.initOwner(parentStage);
		
		grid = new GridPane();
		grid.setPadding(INSETS);
		
		initContent(grid);
		
		scene = new Scene(grid, WIDTH, HEIGHT);
		
		stage.setScene(scene);
		stage.showAndWait();
	}
	
	void initContent(GridPane grid) {
		HBox buttons = new HBox();
		
		buttons.setAlignment(Pos.CENTER);
		
		buttons.getChildren().addAll(saveButton, cancelButton);
		
		grid.add(new Label("Label"), 0, 0);
		//grid.add(new Label("Plural Label"), 0, 1);
		grid.add(new Label("Full Name"), 0, 1);
		grid.add(new Label("Required?"), 0, 2);
		grid.add(new Label("ExternalId?"), 0, 3);
		grid.add(new Label("Unique?"), 0, 4);
		grid.add(new Label("Type"), 0, 5);
		grid.add(new Label("Scale"), 0, 6);
		grid.add(new Label("Precision"), 0, 7);
		grid.add(new Label("Length"), 0, 8);
		grid.add(new Label("Reference To"), 0, 9);
		grid.add(new Label("Relationship Name"), 0, 10);
		grid.add(buttons, 0, 11, 2, 1);
		
		grid.add(label, 1, 0);
		//grid.add(pluralLabel, 1, 1);
		grid.add(fullName, 1, 1);
		grid.add(required, 1, 2);
		grid.add(externalId, 1, 3);
		grid.add(unique, 1, 4);
		grid.add(type, 1, 5);
		grid.add(scale, 1, 6);
		grid.add(precision, 1, 7);
		grid.add(length, 1, 8);
		grid.add(referenceTo, 1, 9);
		grid.add(relationshipName, 1, 10);
	}
	
	void setCustomField(BigObjectCustomField customField) {
		this.customField = customField;
		
		fullName.setText(customField.fullName);
		label.setText(customField.label);
		pluralLabel.setText(customField.pluralLabel);
		referenceTo.setText(customField.referenceTo);
		relationshipName.setText(customField.relationshipName);
		label.setText(customField.label);
		length.setText(String.valueOf(customField.length));
		scale.setText(String.valueOf(customField.scale));
		precision.setText(String.valueOf(customField.length));
		type.setValue(customField.type.name());
		required.setSelected(customField.required);
		externalId.setSelected(customField.externalId);
		unique.setSelected(customField.unique);
	}
}
