package com.app.soqlplus3;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import com.app.soqlplus3.Application.QueryResultRecord;
import com.app.soqlplus3.dialog.ExceptionDialog;

public class SalesforceAsync {
	public Task<Void> task;
	Application app;
	Salesforce salesforce;
	String query;
	Salesforce.QueryContext queryContext;
	
	public static Task<Void> doQuery(Application app, Salesforce salesforce, String query) {
		SalesforceAsync async = new SalesforceAsync(app, salesforce, query);
		
		return async.launch();
	}
	
	public SalesforceAsync(Application app, Salesforce salesforce, String query) {
		this.app = app;
		this.salesforce = salesforce;
		this.query = query;
	}
	
	public Task<Void> launch() {
		task = new Task<Void>() {
			@Override
			public Void call() {
				List<Map<String, Object>> records;
				//Salesforce.QueryContext queryContext;
				QueryResult queryResult;
				
				String resultQuery;
				int iteration = 0;
				int totalRecords = 0;
				
				try {
					Platform.runLater(new Runnable() {
						@Override
						public void run() {					
							app.tableView.getColumns().clear();
							app.tableView.getItems().clear();
						}
					});
					
					//records = salesforce.executeQuery(this, query);
					
					records = new Vector<Map<String, Object>>();
					queryContext = salesforce.new QueryContext();
					queryContext.query = salesforce.normalizeSoqlQuery(query);
					salesforce.parseQueryMedata(query, queryContext);
					
					//
					
					queryResult = salesforce.partnerConnection.query(queryContext.query);
					
					System.out.println("progress 0");
					
					if (queryContext.countRecords != null) {
						updateProgress(0, queryContext.countRecords);
					}
					
					if (queryResult.getSize() > 0) {
						do {
							SObject[] batchRecords;
							
							if (iteration > 0) {
								queryResult = salesforce.partnerConnection.queryMore(queryResult.getQueryLocator());
							}

							batchRecords = queryResult.getRecords();
							
							if (batchRecords != null && batchRecords.length > 0) {
								totalRecords += batchRecords.length;

								System.out.println("progress " + totalRecords);
								
								if (queryContext.countRecords != null) {
									updateProgress(totalRecords, queryContext.countRecords);
								}
								
								for (int recordNo = 0; recordNo < batchRecords.length; recordNo++) {
									SObject record = batchRecords[recordNo];
									Map<String,Object> recordFields = new HashMap<String,Object>();
									
									for (int fieldNo = 0; fieldNo < queryContext.fieldList.size(); fieldNo++) {
										String fieldName;
										Object fieldValue;
										
										fieldName = queryContext.fieldList.get(fieldNo);
										fieldValue = salesforce.getFieldValue(queryContext, record, fieldNo);
										
										recordFields.put(fieldName, fieldValue);
									}
									
									records.add(recordFields);
								}
							}
							
							iteration++;
						} while (queryResult.isDone() == false && isCancelled() == false);
					}
					
					salesforce.lastQueryContext = queryContext;
					
					//
					
					queryContext = salesforce.getLastQueryContext();
					resultQuery = queryContext.query;
					
					app.selectedFieldList = queryContext.fieldList;
					
					app.selectedFieldList.add(0, Application.ROW_NUM);

					
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							app.tableView.getColumns().clear();
							
							for (String field : app.selectedFieldList) {
								TableColumn<QueryResultRecord, String> tableColumn = new TableColumn<QueryResultRecord, String>(
										field);

								tableColumn.setCellValueFactory(
										new Callback<TableColumn.CellDataFeatures<QueryResultRecord, String>, ObservableValue<String>>() {
											@Override
											public ObservableValue<String> call(
													TableColumn.CellDataFeatures<QueryResultRecord, String> p) {
												if (p != null && p.getValue() != null
														&& p.getTableColumn().getText().equalsIgnoreCase(app.ROW_NUM)) {
													return new SimpleStringProperty(String.valueOf(p.getValue().getRowNum()));
												} else {
													if (p.getValue() != null) {
														return new SimpleStringProperty(app.convertObjectToString(
																p.getValue().getValue(p.getTableColumn().getText())));
													} else {
														return new SimpleStringProperty("NULL");
													}
												}
											}
										});

								app.tableView.getColumns().add(tableColumn);
								
								app.tableView.setItems(FXCollections.observableList(app.getQueryResultRecordList(records)));

								app.isQueryRun = true;
								app.buttonExportAs.setDisable(false);
								
								app.executeQueryProgressDialog.getStage().close();
							}
						}
					});
				} catch (Throwable t) {
					Platform.runLater(
						new Runnable() {
							@Override public void run() {
								app.executeQueryProgressDialog.getStage().close();
								
								ExceptionDialog.showDialog("Exception Caught", "Please review your SOQL statement.",
										"Check the exception to understand the error caught during execution of your query.", t, "ok");									
							}
						}
					);
				}
				
				return null;
			}
		};
		
		app.executeQueryProgressDialog.progressBar.progressProperty().bind(task.progressProperty());
		
		task.progressProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				app.executeQueryProgressDialog.setFeedbackText(newValue, queryContext != null && queryContext.countRecords != null ? queryContext.countRecords : null);
			}
		});
		
		new Thread(task).start();
		
		return task;
	}
}
